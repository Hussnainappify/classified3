<?php
App::uses('AppController', 'Controller');
/**
 * Homes Controller
 *
 * @property Home $Home
 * @property PaginatorComponent $Paginator
 */
class HomesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Home->recursive = 0;
		$this->set('homes', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Home->exists($id)) {
			throw new NotFoundException(__('Invalid home'));
		}
		$options = array('conditions' => array('Home.' . $this->Home->primaryKey => $id));
		$this->set('home', $this->Home->find('first', $options));
	}
	public function about() {
		}


/**
 * add method
 *
 * @return void
 */
	
/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	}
