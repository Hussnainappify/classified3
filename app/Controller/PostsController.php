<?php
App::uses('AppController', 'Controller');
/**
 * Posts Controller
 *
 * @property Post $Post
 * @property PaginatorComponent $Paginator
 */
class PostsController extends AppController {


public function beforeFilter() {
    parent::beforeFilter();
  
    $this->Auth->allow('index','view');
}
/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Post->recursive = 0;
		$this->set('posts', $this->Paginator->paginate());
	}

	public function myadd() {

		 $this->set('posts', $this->Post->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {

		if (!$this->Post->exists($id)) {
			throw new NotFoundException(__('Invalid post'));
		}
		$options = array('conditions' => array('Post.' . $this->Post->primaryKey => $id));
		$this->set('post', $this->Post->find('first', $options));
	}

	public function get_images_path($img){
		return ROOT . DS . 'app' .DS. 'webroot' .DS.$img .DS;
		
	}

	public function upload_file($file_ref)
	{

		
		$folder ='uploadjh';

		$base_path = $this->get_images_path($folder);


		$target_file = strtolower($base_path . basename($file_ref["name"]));
	
		
		// debug_it($target_file,1);
	    if (move_uploaded_file($file_ref["tmp_name"], $target_file)) {
	        echo "The file ". basename( $file_ref["name"]). " has been uploaded.";
	    } else {
	   if(is_dir($base_path)){
	   	echo "no create";
	   }else{
	   	echo "create";
	   	 mkdir ($folder, 0755); 
	   }
echo "Your file Upload";
	    }
		

	}
/**
 * add method
 *
 * @return void
 */
	public function add() {

	
			if ($this->request->is('post'))

			 {
			 	
		$post_data = $this->request->data;
    $this->request->data['Post']['user_id'] = $this->Auth->user('id');
    $post_data['user_id'] = $this->Auth->user('id');
    

		if(is_array($_FILES) && !empty($_FILES)){
			//debug_it($_FILES,1);
			foreach ($_FILES as $key => $value) {
				$upload = $this->upload_file($value);
				// $result[$key]['name'] = $value['name'];
				// $result[$key]['result'] = $upload;				
				$post_data[$key] = $value['name'];

			}// end of foreach
					}else{
			echo "error";
		}
		//debug_it($post_data,1);



 
		if ($this->Post->save($post_data)) {
			 
			$this->Flash->success(__('The post has been saved.'));
			return $this->redirect(array('action' => 'index'));
		} else {
			$this->Flash->error(__('The post could not be saved. Please, try again.'));
		}
	}


}
	

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
 public function edit($id = null) {
    if (!$id) {
        throw new NotFoundException(__('Invalid post'));
    }

    $post = $this->Post->findById($id);
    if (!$post) {
        throw new NotFoundException(__('Invalid post'));
    }

    if ($this->request->is(array('post', 'put'))) {
        $this->Post->id = $id;
        if ($this->Post->save($this->request->data)) {
            $this->Flash->success(__('Your post has been updated.'));
            return $this->redirect(array('action' => 'index'));
        }
        $this->Flash->error(__('Unable to update your post.'));
    }

    if (!$this->request->data) {
        $this->request->data = $post;
    }
}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Post->id = $id;
		if (!$this->Post->exists()) {
			throw new NotFoundException(__('Invalid post'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Post->delete()) {
			$this->Flash->success(__('The post has been deleted.'));
		} else {
			$this->Flash->error(__('The post could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}




public function map($id = null) {
		
	}






public function isAuthorized($user) {
    // All registered users can add posts
    if ($this->action === 'add') {
        return true;
    }

    // The owner of a post can edit and delete it
    if (in_array($this->action, array('edit', 'delete'))) {
        $postId = (int) $this->request->params['pass'][0];
        if ($this->Post->isOwnedBy($postId, $user['id'])) {
            return true;
        }
    }

    return parent::isAuthorized($user);
}
}
?>