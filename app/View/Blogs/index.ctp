


<?php
        echo $this->Html->meta('icon');

         echo $this->Html->script('jquery-1.12.4.min');
echo $this->Html->script('plugins');
echo $this->Html->script('custom');
    ?>
<!DOCTYPE html>

<html lang="en" class="no-js">


<head>
  <!-- Basic need -->
  <title>Bach Ly</title>
  <meta charset="UTF-8">
  <meta name="description" content="ReaLand - Real Estate HTML Template">
  <meta name="keywords" content="">
  <meta name="author" content="">
  <link rel="profile" href="#">



</head>

<body>
<div id="page-loader">
    <div class="page-loader__logo">
        <img src="img/uploads/logo_dark.png" alt="ReaLand Logo">
    </div><!-- .page-loader__logo -->
    
    <div class="sk-folding-cube">
        <div class="sk-cube1 sk-cube"></div>
        <div class="sk-cube2 sk-cube"></div>
        <div class="sk-cube4 sk-cube"></div>
        <div class="sk-cube3 sk-cube"></div>
    </div><!-- .sk-folding-cube -->
</div><!-- .page-loader -->
<header class="header header--blue header--top">
  <div class="container">
    <div class="topbar">
      <ul class="topbar__contact">
        <li><span class="ion-ios-telephone-outline topbar__icon"></span><a href="tel:8801234567" class="topbar__link">(880)-123-4567</a></li>
        <li><span class="ion-ios-email-outline topbar__icon"></span><a href="mailto:ReaLand@support.com" class="topbar__link">ReaLand@support.com</a></li>
      </ul><!-- .topbar__contact -->

      <ul class="topbar__user">
        <span class="ion-ios-person-outline topbar__user-icon"></span>
        <?php echo $this->Html->link("Signin", array('controller' => 'users','action'=> 'login'), array( 'class' => 'topbar__link'))?>
        
      </ul>
    </div><!-- .topbar -->

    <div class="header__main">
      <div class="header__logo">
        <a href="index-2.html">
          <h1 class="screen-reader-text">Realand</h1>
          <img src="img/uploads/logo.png" alt="Realand">
        </a>
      </div><!-- .header__main -->

      <div class="nav-mobile">
        <a href="#" class="nav-toggle">
          <span></span>
        </a><!-- .nav-toggle -->
      </div><!-- .nav-mobile -->

      <div class="header__menu header__menu--v1">
        <ul class="header__nav">
          <li class="header__nav-item">
          
            <?php echo $this->Html->link("Homes", array('controller' => 'Homes','action'=> 'index'), array( 'class' => 'header__nav-link'))?>
          </li>
          
          <li class="header__nav-item">
          
            <?php echo $this->Html->link("About Us", array('controller' => 'Homes','action'=> 'about'), array( 'class' => 'header__nav-link'))?>
          </li>
            
          </li>
          
          <li class="header__nav-item">
          
            <?php echo $this->Html->link("Ads", array('controller' => 'Posts','action'=> 'index'), array( 'class' => 'header__nav-link'))?>
          </li>
          <li class="header__nav-item">
            <a href="contact.html" class="header__nav-link">Contact Us</a>
            
          </li>
          
          
              
          
            
          <li class="header__nav-item">
          
            <?php echo $this->Html->link("Blog", array('controller' => 'Blogs','action'=> 'index'), array( 'class' => 'header__nav-link'))?>
          </li>
        </ul><!-- .header__nav -->

        <ul class="topbar__user">
          <li><a href="sign_up.html" class="topbar__link">Sign In</a></li>
          <li><a href="sign_up.html" class="topbar__link">Join</a></li>
        </ul>

      </div><!-- .header__menu -->
<?php echo $this->Html->link("+  Submit Add", array('controller' => 'posts','action'=> 'add'), array( 'class' => 'header__cta'))?>
  
    </div><!-- .header__main -->
  </div><!-- .container -->
</header><!-- .header -->
<section class="blog" style="margin-top:200px">
  <div class="container">
    <ul class="ht-breadcrumbs ht-breadcrumbs--y-padding ht-breadcrumbs--b-border">
      <li class="ht-breadcrumbs__item"><a href="#" class="ht-breadcrumbs__link"><span class="ht-breadcrumbs__title">Home</span></a></li>
      <li class="ht-breadcrumbs__item"><a href="#" class="ht-breadcrumbs__link"><span class="ht-breadcrumbs__title">Pages</span></a></li>
      <li class="ht-breadcrumbs__item"><span class="ht-breadcrumbs__page">Blog</span></li>
    </ul><!-- ht-breadcrumb -->

    <div class="blog__main">
      <div class="row">
        <main class="col-md-8 col-md-main">
          <div class="row">
            <div class="col-md-6">
              <article class="news__single--v2 news__single--b-margin">
                <div class="news__thumbnail">
                  <a href="blog_single.html">
                    <div class="news__thumbnail-overlay"></div><!-- .news__thumbnail-overlay -->
                  
                        <?php echo $this->Html->image('uploads/news_1.jpg');?>
                  </a>
                  <span class="item__label">Pinned</span>
                </div><!-- .news_thumbnail -->
    
                <div class="news__content">
                  <div class="news__body">
                    <span class="news__meta news__meta-time"><span class="ion-ios-calendar-outline"></span> July 21, 2017</span>
                    <h3 class="news__title"><a href="blog_single.html">Releases 2Q 2017 Tokyo, Japan</a></h3>
                    <p class="news__excerpt news__excerpt--v2">The upper level has three bedrooms, with the main bedroom having built-in wardrobes and an ensuite with his-and-hers sinks, underfloor heating and shower dosis...</p>
                  </div><!-- .news__body -->
    
                  <div class="news__footer">
                    <span class="news__meta news__meta-comments"><span class="ion-ios-chatboxes-outline"></span> 3 Comment(s)</span>
                    <a href="blog_single.html" class="news__readmore">&plus; Read More</a>
                  </div><!-- .news__footer -->
                </div><!-- .news__content -->
              </article><!-- news__single--v2 -->
            </div><!-- .col -->

            <div class="col-md-6">
              <article class="news__single--v2 news__single--b-margin">
                <div class="news__thumbnail">
                  <a href="blog_single.html">
                    <div class="news__thumbnail-overlay"></div><!-- .news__thumbnail-overlay -->
                  <?php echo $this->Html->image('uploads/news_2.jpg');?>
                  </a>
                </div><!-- .news_thumbnail -->
    
                <div class="news__content">
                  <div class="news__body">
                    <span class="news__meta news__meta-time"><span class="ion-ios-calendar-outline"></span> July 21, 2017</span>
                    <h3 class="news__title"><a href="blog_single.html">Releases 2Q 2017 Tokyo, Japan</a></h3>
                    <p class="news__excerpt news__excerpt--v2">The upper level has three bedrooms, with the main bedroom having built-in wardrobes and an ensuite with his-and-hers sinks, underfloor heating and shower dosis...</p>
                  </div><!-- .news__body -->
    
                  <div class="news__footer">
                    <span class="news__meta news__meta-comments"><span class="ion-ios-chatboxes-outline"></span> 3 Comment(s)</span>
                    <a href="blog_single.html" class="news__readmore">&plus; Read More</a>
                  </div><!-- .news__footer -->
                </div><!-- .news__content -->
              </article><!-- news__single--v2 -->
            </div><!-- .col -->

            <div class="col-md-6">
              <article class="news__single--v2 news__single--b-margin">
                <div class="news__thumbnail">
                  <a href="blog_single.html">
                    <div class="news__thumbnail-overlay"></div><!-- .news__thumbnail-overlay -->
                 <?php echo $this->Html->image('uploads/news_3.jpg');?>
                  </a>
                </div><!-- .news_thumbnail -->
    
                <div class="news__content">
                  <div class="news__body">
                    <span class="news__meta news__meta-time"><span class="ion-ios-calendar-outline"></span> July 21, 2017</span>
                    <h3 class="news__title"><a href="blog_single.html">Releases 2Q 2017 Tokyo, Japan</a></h3>
                    <p class="news__excerpt news__excerpt--v2">The upper level has three bedrooms, with the main bedroom having built-in wardrobes and an ensuite with his-and-hers sinks, underfloor heating and shower dosis...</p>
                  </div><!-- .news__body -->
    
                  <div class="news__footer">
                    <span class="news__meta news__meta-comments"><span class="ion-ios-chatboxes-outline"></span> 3 Comment(s)</span>
                    <a href="blog_single.html" class="news__readmore">&plus; Read More</a>
                  </div><!-- .news__footer -->
                </div><!-- .news__content -->
              </article><!-- news__single--v2 -->
            </div><!-- .col -->

            <div class="col-md-6">
              <article class="news__single--v2 news__single--b-margin">
                <div class="news__thumbnail">
                  <a href="blog_single.html">
                    <div class="news__thumbnail-overlay"></div><!-- .news__thumbnail-overlay -->
                     <?php echo $this->Html->image('uploads/news_4.jpg');?>
                  </a>
                </div><!-- .news_thumbnail -->
    
                <div class="news__content">
                  <div class="news__body">
                    <span class="news__meta news__meta-time"><span class="ion-ios-calendar-outline"></span> July 21, 2017</span>
                    <h3 class="news__title"><a href="blog_single.html">Releases 2Q 2017 Tokyo, Japan</a></h3>
                    <p class="news__excerpt news__excerpt--v2">The upper level has three bedrooms, with the main bedroom having built-in wardrobes and an ensuite with his-and-hers sinks, underfloor heating and shower dosis...</p>
                  </div><!-- .news__body -->
    
                  <div class="news__footer">
                    <span class="news__meta news__meta-comments"><span class="ion-ios-chatboxes-outline"></span> 3 Comment(s)</span>
                    <a href="blog_single.html" class="news__readmore">&plus; Read More</a>
                  </div><!-- .news__footer -->
                </div><!-- .news__content -->
              </article><!-- news__single--v2 -->
            </div><!-- .col -->

       

      