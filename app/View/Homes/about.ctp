


<?php
        echo $this->Html->meta('icon');

         echo $this->Html->script('jquery-1.12.4.min');
echo $this->Html->script('plugins');
echo $this->Html->script('custom');
    ?>
<!DOCTYPE html>

<html lang="en" class="no-js">


<head>
  <!-- Basic need -->
  <title>Bach Ly</title>
  <meta charset="UTF-8">
  <meta name="description" content="ReaLand - Real Estate HTML Template">
  <meta name="keywords" content="">
  <meta name="author" content="">
  <link rel="profile" href="#">



</head>

<body>
<div id="page-loader">
    <div class="page-loader__logo">
        <img src="img/uploads/logo_dark.png" alt="ReaLand Logo">
    </div><!-- .page-loader__logo -->
    
    <div class="sk-folding-cube">
        <div class="sk-cube1 sk-cube"></div>
        <div class="sk-cube2 sk-cube"></div>
        <div class="sk-cube4 sk-cube"></div>
        <div class="sk-cube3 sk-cube"></div>
    </div><!-- .sk-folding-cube -->
</div><!-- .page-loader -->
<header class="header header--blue header--top">
  <div class="container">
    <div class="topbar">
      <ul class="topbar__contact">
        <li><span class="ion-ios-telephone-outline topbar__icon"></span><a href="tel:8801234567" class="topbar__link">(880)-123-4567</a></li>
        <li><span class="ion-ios-email-outline topbar__icon"></span><a href="mailto:ReaLand@support.com" class="topbar__link">ReaLand@support.com</a></li>
      </ul><!-- .topbar__contact -->

      <ul class="topbar__user">
        <span class="ion-ios-person-outline topbar__user-icon"></span>
        <?php echo $this->Html->link("Signin", array('controller' => 'users','action'=> 'login'), array( 'class' => 'topbar__link'))?>
        
      </ul>
    </div><!-- .topbar -->

    <div class="header__main">
      <div class="header__logo">
        <a href="index-2.html">
          <h1 class="screen-reader-text">Realand</h1>
          <img src="img/uploads/logo.png" alt="Realand">
        </a>
      </div><!-- .header__main -->

      <div class="nav-mobile">
        <a href="#" class="nav-toggle">
          <span></span>
        </a><!-- .nav-toggle -->
      </div><!-- .nav-mobile -->

      <div class="header__menu header__menu--v1">
        <ul class="header__nav">
          <li class="header__nav-item">
          
            <?php echo $this->Html->link("Homes", array('controller' => 'Posts','action'=> 'index'), array( 'class' => 'header__nav-link'))?>
          </li>
          
          <li class="header__nav-item">
          
            <?php echo $this->Html->link("About Us", array('controller' => 'Homes','action'=> 'about'), array( 'class' => 'header__nav-link'))?>
          </li>
            
          </li>
          
          <li class="header__nav-item">
          
            <?php echo $this->Html->link("Ads", array('controller' => 'Posts','action'=> 'index'), array( 'class' => 'header__nav-link'))?>
          </li>
          <li class="header__nav-item">
            <a href="contact.html" class="header__nav-link">Contact Us</a>
            
          </li>
          
          
              
          
            
          <li class="header__nav-item">
          
            <?php echo $this->Html->link("Blog", array('controller' => 'Blogs','action'=> 'index'), array( 'class' => 'header__nav-link'))?>
          </li>
        </ul><!-- .header__nav -->

        <ul class="topbar__user">
          <li><a href="sign_up.html" class="topbar__link">Sign In</a></li>
          <li><a href="sign_up.html" class="topbar__link">Join</a></li>
        </ul>

      </div><!-- .header__menu -->
<?php echo $this->Html->link("+  Submit Add", array('controller' => 'posts','action'=> 'add'), array( 'class' => 'header__cta'))?>
  
    </div><!-- .header__main -->
  </div><!-- .container -->
</header><!-- .header -->
<section class="about-us" style="margin-top:200px">
  <div class="container">
    <ul class="ht-breadcrumbs ht-breadcrumbs--y-padding ht-breadcrumbs--b-border">
      <li class="ht-breadcrumbs__item"><a href="#" class="ht-breadcrumbs__link"><span class="ht-breadcrumbs__title">Home</span></a></li>
      <li class="ht-breadcrumbs__item"><a href="#" class="ht-breadcrumbs__link"><span class="ht-breadcrumbs__title">Pages</span></a></li>
      <li class="ht-breadcrumbs__item"><span class="ht-breadcrumbs__page">About Us</span></li>
    </ul><!-- ht-breadcrumb -->

    <div class="about-us__main">
      <div class="row">
        <main class="col-md-8 col-md-main">
          <div class="about-us__img">
               <?php echo $this->Html->image('uploads/about_us.jpg');?>
         
          </div><!-- .about-us__img -->
          <h1 class="about-us__title">We are ReaLand</h1>
          <p>
            Our mission is to redefine real estate in the customer's favor.
          </p>
          <p>
            ReaLand got its start inventing map-based search. Everyone told us the easy money was in running ads for traditional brokers, but we couldn't stop thinking about how different real estate would be if it were designed from the ground up, using technology and totally different values, to put customers first. So we joined forces with agents who wanted to be customer advocates, not salesmen.
          </p>
          <p>
            Since these were our own agents, we could survey each customer on our service and pay a bonus based on the review. We deepened our technology beyond the initial search to make the home tour, the listing debut, the escrow process, the whole process, faster, easier and worry-free. And we gave customers more value, not just by saving each thousands in fees, but by investing in every home we sell, by measuring our performance and improving constantly.
          </p>

          <p>
            This is how real estate would be if it were designed just for you, because, well, it was.
          </p>
        </main><!-- .col -->
        <aside class="col-md-4 col-md-sidebar">
          <section class="widget">
            <form class="contact-form contact-form--bordered contact-form--wild-sand">
              <div class="contact-form__header">
                <h3 class="contact-form__title">Drop Us a Line</h3>
              </div><!-- .contact-form__header -->
              
              <div class="contact-form__body">
                <input type="text" class="contact-form__field" placeholder="Name" name="name" required>
                <input type="email" class="contact-form__field" placeholder="Email" name="email" required>
                <input type="tel" class="contact-form__field" placeholder="Phone number" name="phone number">
                <textarea class="contact-form__field contact-form__comment" cols="30" rows="4" placeholder="Comment" name="comment" required></textarea>
              </div><!-- .contact-form__body -->

              <div class="contact-form__footer">
                <input type="submit" class="contact-form__submit" name="submit" value="Send Message">
              </div><!-- .contact-form__footer -->
            </form><!-- .contact-form -->
          </section><!-- .widget -->

          <section class="widget widget--wild-sand widget--padding-20 widget__news">
            <h3 class="widget__title">Get to Know</h3>
            <ul class="widget__news-list">
              <li class="widget__news-item"><a href="#">Outer Sunset Real Estate: <span>San Francisco Neighborhood Guide</span></a></li>
              <li class="widget__news-item"><a href="#">Pacific Heights Real Estate: <span>San Francisco CA Neighborhood</span></a></li>
              <li class="widget__news-item"><a href="#">Mission District San Francisco: <span>Authentic Community</span></a></li>
              <li class="widget__news-item"><a href="#">Hayes Valley Real Estate: <span>San Francisco CA Neighborhood</span></a></li>
            </ul>
          </section><!-- .widget -->
        </aside><!-- .col -->
      </div><!-- .row -->
    </div><!-- .about-us__main -->
  </div><!-- .container -->
</section><!-- .about-us -->

	<div class="footer__main">
		<div class="container">
			<div class="footer__logo">
				<h1 class="screen-reader-text">ReaLand</h1>
				   <?php echo $this->Html->image('uploads/logo.png');?>
			</div><!-- .footer__logo -->
			<p class="footer__desc">ReaLand is made for buying and selling houses faster, easier and customized for you.</p>
			<ul class="footer__social">
				<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
			</ul><!-- .footer__social -->
		</div><!-- .container -->
	</div><!-- .footer__main -->

	
</footer><!-- .footer -->



<!-- Mirrored from haintheme.com/demo/html/realand/about_us.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 23 Apr 2019 11:51:52 GMT -->
</html>