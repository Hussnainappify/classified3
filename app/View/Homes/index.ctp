


<?php
        echo $this->Html->meta('icon');

         echo $this->Html->script('jquery-1.12.4.min');
echo $this->Html->script('plugins');
echo $this->Html->script('custom');
    ?>
<!DOCTYPE html>

<html lang="en" class="no-js">


<head>
	<!-- Basic need -->
	<title>Bach Ly</title>
	<meta charset="UTF-8">
	<meta name="description" content="ReaLand - Real Estate HTML Template">
	<meta name="keywords" content="">
	<meta name="author" content="">
	<link rel="profile" href="#">



</head>



<body>
<div id="page-loader">
    <div class="page-loader__logo">
        <img src="img/uploads/logo_dark.png" alt="ReaLand Logo">
    </div><!-- .page-loader__logo -->
    
    <div class="sk-folding-cube">
        <div class="sk-cube1 sk-cube"></div>
        <div class="sk-cube2 sk-cube"></div>
        <div class="sk-cube4 sk-cube"></div>
        <div class="sk-cube3 sk-cube"></div>
    </div><!-- .sk-folding-cube -->
</div><!-- .page-loader -->
<header class="header header--blue header--top">
	<div class="container">
		<div class="topbar">
			<ul class="topbar__contact">
				<li><span class="ion-ios-telephone-outline topbar__icon"></span><a href="tel:8801234567" class="topbar__link">(880)-123-4567</a></li>
				<li><span class="ion-ios-email-outline topbar__icon"></span><a href="mailto:ReaLand@support.com" class="topbar__link">ReaLand@support.com</a></li>
			</ul><!-- .topbar__contact -->

			<ul class="topbar__user">
				<span class="ion-ios-person-outline topbar__user-icon"></span>
				<?php echo $this->Html->link("Signin", array('controller' => 'users','action'=> 'login'), array( 'class' => 'topbar__link'))?>
				
			</ul>
		</div><!-- .topbar -->

		<div class="header__main">
			<div class="header__logo">
				<a href="index-2.html">
					<h1 class="screen-reader-text">Realand</h1>
					<img src="img/uploads/logo.png" alt="Realand">
				</a>
			</div><!-- .header__main -->

			<div class="nav-mobile">
				<a href="#" class="nav-toggle">
					<span></span>
				</a><!-- .nav-toggle -->
			</div><!-- .nav-mobile -->

			<div class="header__menu header__menu--v1">
				<ul class="header__nav">
					<li class="header__nav-item">
					
						<?php echo $this->Html->link("Homes", array('controller' => 'Homes','action'=> 'index'), array( 'class' => 'header__nav-link'))?>
					</li>
					
					<li class="header__nav-item">
					
						<?php echo $this->Html->link("About Us", array('controller' => 'Homes','action'=> 'about'), array( 'class' => 'header__nav-link'))?>
					</li>
						
					</li>
					
					<li class="header__nav-item">
					
						<?php echo $this->Html->link("Ads", array('controller' => 'Posts','action'=> 'index'), array( 'class' => 'header__nav-link'))?>
					</li>
					<li class="header__nav-item">
						<a href="contact.html" class="header__nav-link">Contact Us</a>
						
					</li>
					
					
							
					
						
					<li class="header__nav-item">
					
						<?php echo $this->Html->link("Blog", array('controller' => 'Blogs','action'=> 'index'), array( 'class' => 'header__nav-link'))?>
					</li>
				</ul><!-- .header__nav -->

				<ul class="topbar__user">
					<li><a href="sign_up.html" class="topbar__link">Sign In</a></li>
					<li><a href="sign_up.html" class="topbar__link">Join</a></li>
				</ul>

			</div><!-- .header__menu -->
<?php echo $this->Html->link("+  Submit Add", array('controller' => 'posts','action'=> 'add'), array( 'class' => 'header__cta'))?>
	
		</div><!-- .header__main -->
	</div><!-- .container -->
</header><!-- .header -->

<section class="main-slider">
	<div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="realand-slider-1" data-source="gallery" style="margin:0px auto;background:rgba(0,0,0,0.5);padding:0px;margin-top:0px;margin-bottom:0px;">
		<!-- START REVOLUTION SLIDER 5.4.6 fullwidth mode -->
		<div id="rev_slider_1_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.6">
			<ul>
				<!-- SLIDE  -->
				<li data-index="rs-1" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="img/revslider/main_slider_1-100x50.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
					<!-- MAIN IMAGE -->
					<img src="img/revslider/dummy.png" alt="" title="main_slider_1" width="1920" height="820" data-lazyload="images/revslider/main_slider_1.jpg" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
					<!-- LAYERS -->
	
					<!-- LAYER NR. 1 -->
					<div class="tp-caption   tp-resizeme" id="slide-1-layer-1" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['-68','-68','-60','-40']" data-fontsize="['18','18','16','14']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":300,"speed":1500,"frame":"0","from":"y:top;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5; white-space: nowrap; font-size: 18px; line-height: 22px; font-weight: 400; color: #f3f3f3; letter-spacing: 0px;font-family:Roboto;">THE BEST REAL ESTATE DEALS </div>
	
					<!-- LAYER NR. 2 -->
					<div class="tp-caption   tp-resizeme" id="slide-1-layer-2" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-fontsize="['60','60','40','26']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":300,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; white-space: nowrap; font-size: 60px; line-height: 22px; font-weight: 900; color: #ffffff; letter-spacing: 0px;font-family:Raleway;">Discover Your Perfect Home </div>
	
					<!-- LAYER NR. 3 -->
					<a href="https://www.youtube.com/watch?v=Q0CbN8sfihY" data-lity class="tp-caption   tp-resizeme" target="_self" id="slide-1-layer-7" data-x="['center','center','center','center']" data-hoffset="['6','6','6','6']" data-y="['top','top','top','top']" data-voffset="['480','380','300','230']" data-fontsize="['60','60','36','36']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-actions='' data-responsive_offset="on" data-frames='[{"delay":300,"speed":1500,"frame":"0","from":"y:bottom;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 7; white-space: nowrap; font-size: 60px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Open Sans;text-decoration: none;"><i class="fa fa-play-circle-o"></i> </a>
				</li>
				<!-- SLIDE  -->
				<li data-index="rs-12" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="img/revslider/main_slider_1-2-100x50.jpeg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
					<!-- MAIN IMAGE -->
					<img src="img/revslider/dummy.png" alt="" title="Main Slider 1-2" width="1920" height="820" data-lazyload="img/revslider/main_slider_1-2.jpeg" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
					<!-- LAYERS -->
	
					<!-- LAYER NR. 4 -->
					<div class="tp-caption   tp-resizeme" id="slide-12-layer-1" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['-68','-68','-60','-40']" data-fontsize="['18','18','16','14']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":300,"speed":1500,"frame":"0","from":"y:top;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5; white-space: nowrap; font-size: 18px; line-height: 22px; font-weight: 400; color: #f3f3f3; letter-spacing: 0px;font-family:Roboto;">THE BEST REAL ESTATE DEALS </div>
	
					<!-- LAYER NR. 5 -->
					<div class="tp-caption   tp-resizeme" id="slide-12-layer-2" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-fontsize="['60','60','40','26']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":300,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; white-space: nowrap; font-size: 60px; line-height: 22px; font-weight: 900; color: #ffffff; letter-spacing: 0px;font-family:Raleway;">Discover Your Perfect Home </div>
	
					<!-- LAYER NR. 6 -->
					<a href="https://www.youtube.com/watch?v=Q0CbN8sfihY" data-lity class="tp-caption   tp-resizeme" target="_self" id="slide-12-layer-7" data-x="['center','center','center','center']" data-hoffset="['6','6','6','6']" data-y="['top','top','top','top']" data-voffset="['480','380','300','230']" data-fontsize="['60','60','36','36']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-actions='' data-responsive_offset="on" data-frames='[{"delay":300,"speed":1500,"frame":"0","from":"y:bottom;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 7; white-space: nowrap; font-size: 60px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Open Sans;text-decoration: none;"><i class="fa fa-play-circle-o"></i> </a>
				</li>
			</ul>
			<div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
		</div>
	</div>
	<!-- END REVOLUTION SLIDER -->

	
		<section  class="item-grid">
  <div class="container">
    <div class="row">
    	<?php foreach ($posts as $post): ?>
<div class="col-md-6 item-grid__container">
        <div class="listing">
          <div class="item-grid__image-container">
            <a href="single_property_1.html">
              <div class="item-grid__image-overlay"></div><!-- .item-grid__image-overlay -->
             <img src="img/uploads/dream_house_take_away.jpg" alt="Dream House Take Away" class="listing__img">
              <span class="listing__favorite"><i class="fa fa-heart-o" aria-hidden="true"></i></span>
            </a>
          </div><!-- .item-grid__image-container -->

          <div class="item-grid__content-container">
            <div class="listing__content">
              <div class="listing__header">
                <div class="listing__header-primary">
                  <h3 class="listing__title"><?php echo h($post['Post']['title']); ?></a></h3>
                  <p class="listing__location"><span class="ion-ios-location-outline listing__location-icon"></span> <?php echo h($post['Post']['adress']); ?></p>
                </div><!-- .listing__header-primary -->
                <p class="listing__price">$<?php echo h($post['Post']['price']); ?></p>
              </div><!-- .listing__header -->
              <div class="listing__details">
                <ul class="listing__stats">
                  <li><span class="listing__figure">5<sup>&plus;</sup></span> Beds</li>
                  <li><span class="listing__figure">3</span> Baths</li>
                  <li><span class="listing__figure">1,250</span> sq.ft</li>
                </ul><!-- .listing__stats -->
                <a  class="listing__btn"<?php echo $this->Html->link(__('View'), array('action' => 'view',$post['Post']['id']));?></a>
              </div><!-- .listing__details -->
            </div><!-- .listing-content -->
          </div><!-- .item-grid__content-container -->
        </div><!-- .listing -->
      </div><!-- .col -->

 <?php endforeach; ?>

    </div>
</div>
</section>
    	


<section class="features">
  <div class="features__overlay">
    <div class="container">
      <div class="row">
        <div class="col-sm-4">
          <div class="feature">
            <img src="img/icon_map.png" alt="Map" class="feature__icon">
            <h3 class="feature__title">Freshest Market Info</h3>
            <p class="feature__desc">
              Our extensive database of listings and market info provide the most accurate.
            </p>
          </div><!-- .feature -->
        </div><!-- .col -->

        <div class="col-sm-4">
          <div class="feature">
            <img src="img/icon_search.png" alt="Search" class="feature__icon">
            <h3 class="feature__title">Top Local Agents</h3>
            <p class="feature__desc">
              Our extensive database of listings and market info provide the most accurate.
            </p>
          </div><!-- .feature -->
        </div><!-- .col -->
        <div class="col-sm-4">
          <div class="feature">
            <img src="img/icon_negotiation.png" alt="Negotiation" class="feature__icon">
            <h3 class="feature__title">Peace of Mind</h3>
            <p class="feature__desc">
              Our extensive database of listings and market info provide the most accurate.
            </p>
          </div><!-- .feature -->
        </div><!-- .col -->  
      </div><!-- .row -->
    </div><!-- .container -->
  </div><!-- .features__overlay -->
</section><!-- .features -->
<section class="featured-listing">
  <div class="container">
    <h2 class="section__title">Featured Listing</h2>
    <div class="row">
      <div class="col-md-4 featured-listing__container">
        <div class="listing">
          <div class="item-grid__image-container">
            <a href="single_property_1.html">
              <div class="item-grid__image-overlay"></div><!-- .item-grid__image-overlay -->
              <img src="img/uploads/real_house_luxury_villa.jpg" alt="Real House Luxury Villa" class="listing__img">
              <span class="listing__favorite"><i class="fa fa-heart-o" aria-hidden="true"></i></span>
            </a>
          </div><!-- item-grid__image-container -->

          <div class="item-grid__content-container">
            <div class="listing__content">
              <h3 class="listing__title"><a href="single_property_1.html">Real House Luxury Villa</a></h3>
              <p class="listing__location"><span class="ion-ios-location-outline listing__location-icon"></span> 157 West 57th St, 77 - Central Park South, NYC</p>
              <p class="listing__price">$2,285,500</p>
              <div class="listing__details">
                <ul class="listing__stats">
                  <li><span class="listing__figure">5<sup>&plus;</sup></span> Beds</li>
                  <li><span class="listing__figure">3</span> Baths</li>
                  <li><span class="listing__figure">1,250</span> sq.ft</li>
                </ul><!-- .listing__stats -->
                <a href="single_property_1.html" class="listing__btn">Details <span class="listing__btn-icon"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
              </div><!-- .listing__details -->
            </div><!-- .listing-content -->
          </div><!-- .item-grid__content-container -->
        </div><!-- .listing -->
      </div><!-- .col -->

      <div class="col-md-4 featured-listing__container">
        <div class="listing">
          <div class="item-grid__image-container">
            <a href="single_property_1.html">
              <div class="item-grid__image-overlay"></div><!-- .item-grid__image-overlay -->
              <img src="img/uploads/dream_house_take_away.jpg" alt="Dream House Take Away" class="listing__img">
              <span class="listing__favorite"><i class="fa fa-heart-o" aria-hidden="true"></i></span>
            </a>
          </div><!-- item-grid__image-container -->

          <div class="item-grid__content-container">
            <div class="listing__content">
              <h3 class="listing__title"><a href="single_property_1.html">House Take Away</a></h3>
              <p class="listing__location"><span class="ion-ios-location-outline listing__location-icon"></span> 157 West 57th St, 77 - Central Park South, NYC</p>
              <p class="listing__price">$85,500/ <span class="listing__price--small">Month</span></p>
              <div class="listing__details">
                <ul class="listing__stats">
                  <li><span class="listing__figure">5<sup>&plus;</sup></span> Beds</li>
                  <li><span class="listing__figure">3</span> Baths</li>
                  <li><span class="listing__figure">1,250</span> sq.ft</li>
                </ul><!-- .listing__stats -->
                <a href="single_property_1.html" class="listing__btn">Details <span class="listing__btn-icon"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
              </div><!-- .listing__details -->
            </div><!-- .listing-content -->
          </div><!-- .item-grid__content-container -->
        </div><!-- .listing -->
      </div><!-- .col -->

      <div class="col-md-4 featured-listing__container">
        <div class="listing">
          <div class="item-grid__image-container">
            <a href="single_property_1.html">
              <div class="item-grid__image-overlay"></div><!-- .item-grid__image-overlay -->
              <img src="img/uploads/weston_hightpointe_place.jpg" alt="Weston Hightpointe Place" class="listing__img">
              <span class="listing__favorite"><i class="fa fa-heart-o" aria-hidden="true"></i></span>
            </a>
          </div><!-- .item-grid__image-container -->

          <div class="item-grid__content-container">
            <div class="listing__content">
              <h3 class="listing__title"><a href="single_property_1.html">Weston Hightpointe Place</a></h3>
              <p class="listing__location"><span class="ion-ios-location-outline listing__location-icon"></span> 157 West 57th St, 77 - Central Park South, NYC</p>
              <p class="listing__price"><a href="#">Request Price</a></p>
              <div class="listing__details">
                <ul class="listing__stats">
                  <li><span class="listing__figure">5<sup>&plus;</sup></span> Beds</li>
                  <li><span class="listing__figure">3</span> Baths</li>
                  <li><span class="listing__figure">1,250</span> sq.ft</li>
                </ul><!-- .listing__stats -->
                <a href="single_property_1.html" class="listing__btn">Details <span class="listing__btn-icon"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
              </div><!-- .listing__details -->
            </div><!-- .listing-content -->
          </div><!-- .item-grid__content-container -->
        </div><!-- .listing -->
      </div><!-- .col -->
    </div><!-- .row -->
  </div><!-- .container -->
</section><!-- .featured-listing -->
<section class="testimonial">
  <div class="container">
    <div class="testimonial__container">
      <div class="row">
        <div class="col-md-6">
          <div class="testimonial__video">
            <img src="img/uploads/testimonial_1.jpg" alt="Testimonial" class="testimonial__video-img">
            <a href="https://www.youtube.com/watch?v=Q0CbN8sfihY" data-lity>
              <i class="fa fa-play-circle-o play-icon" aria-hidden="true"></i>
            </a>
            <div class="testimonial__overlay"></div><!-- .testimonial__overlay -->
          </div><!-- .testimonial__video -->
        </div><!-- .col -->

        <div class="col-md-6">
          <div class="testimonial__content">
            <h2 class="section__title">2000&plus; Happy Clients</h2>
            <p class="testimonial__desc">Realand has recently sold our house and home. We have found that it was one of the best investments we could have made, as nothing was a problem to her, and it certainly took the worry of marketing and selling off our mind, as ReaLand had everything under control.</p>
            <div class="testimonial__customer">
              <img src="img/uploads/testimonial_avatar.png" alt="Karty and Will">
              <div class="testimonial__customer-profile">
                <h4 class="testimonial__customer-name">Karty & Will</h4>
                <p class="testimonial__customer-company">Sunderland, Haintheme</p>
                <img src="img/rating.png" alt="Five Stars Rating">
              </div><!-- .testimonial__customer-profile -->
            </div><!-- .testimonial__customer -->
          </div><!-- .testimonial__content -->
        </div><!-- .col -->
      </div><!-- .row -->
    </div><!-- .testimonial__container -->
  </div><!-- .container -->
</section><!-- .testimonial -->
<section class="partners">
  <div class="container">
    <ul class="partners__list">
      <li><img src="img/uploads/partner1.jpg" alt="Partner"></li>
      <li><img src="img/uploads/partner2.jpg" alt="Partner"></li>
      <li><img src="img/uploads/partner3.jpg" alt="Partner"></li>
      <li><img src="img/uploads/partner4.jpg" alt="Partner"></li>
      <li><img src="img/uploads/partner5.jpg" alt="Partner"></li>
    </ul><!-- .partners__list -->
  </div><!-- .container -->
</section><!-- .partners -->
<section class="news">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="news__single">
          <div class="news__time">
            <span class="news__date">26</span>
            <span class="news__my">July, 2017</span>
          </div><!-- .news__time -->

          <div class="news__main">
            <h3 class="news__title"><a href="blog_single.html">Releases 2Q 2017 Tokyo, Japan</a></h3>
            <span class="news__meta"><span class="ion-ios-chatboxes-outline"></span> 3 Comment(s)</span>
            <p class="news__excerpt">The upper level has three bedrooms, with the main bedroom having built-in wardrobes and an ensuite with...</p>
            <a href="blog_single.html" class="news__readmore">&plus; Read More</a>
          </div><!-- .news__main-->
        </div><!-- .news__single -->
      </div><!-- .col -->

      <div class="col-md-6">
        <div class="news__single">
          <div class="news__time">
            <span class="news__date">27</span>
            <span class="news__my">July, 2017</span>
          </div><!-- .news__time -->

          <div class="news__main">
            <h3 class="news__title"><a href="blog_single.html">Releases 2Q 2017 Tokyo, Japan</a></h3>
            <span class="news__meta"><span class="ion-ios-chatboxes-outline"></span> 3 Comment(s)</span>
            <p class="news__excerpt">The upper level has three bedrooms, with the main bedroom having built-in wardrobes and an ensuite with...</p>
            <a href="blog_single.html" class="news__readmore">&plus; Read More</a>
          </div><!-- .news__main-->
        </div><!-- .news__single -->
      </div><!-- .col -->
    </div><!-- .row -->
  </div><!-- .container -->
</section><!-- .news -->

<footer class="footer">
		<div class="footer__links">
			<div class="container">
				<div class="row">
					
	<div class="footer__main">
		<div class="container">
			<div class="footer__logo">
				<h1 class="screen-reader-text">ReaLand</h1>
				<img src="img/uploads/logo_dark.png" alt="ReaLand">
			</div><!-- .footer__logo -->
			<p class="footer__desc">ReaLand is made for buying and selling houses faster, easier and customized for you.</p>
			<ul class="footer__social">
				<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
			</ul><!-- .footer__social -->
		</div><!-- .container -->
	</div><!-- .footer__main -->

	<div class="footer__copyright">
		<div class="container">
			<div class="footer__copyright-inner">
				<p class="footer__copyright-desc">
					&copy; 2017 <span class="footer--highlighted">ReaLand</span> Real Estate. All Right Reserved.
				</p>
				<ul class="footer__copyright-list">
					<li><a href="#">Neighborhood Guide</a></li>
					<li><a href="#">Market Trends</a></li>
					<li><a href="#">Schools</a></li>
					<li><a href="#">Real Estate Tips</a></li>
				</ul>
			</div><!-- .footer__copyright-inner -->
		</div><!-- .container -->
	</div><!-- .footer__copyright -->
</footer><!-- .footer -->
<a href="#" class="back-to-top"><span class="ion-ios-arrow-up"></span></a>



<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDyCxHyc8z9gMA5IlipXpt0c33Ajzqix4"></script>
<script src="../../../../cdn.rawgit.com/googlemaps/v3-utility-library/master/infobox/src/infobox.js"></script>

</body>

<!-- Mirrored from haintheme.com/demo/html/realand/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 23 Apr 2019 11:44:47 GMT -->
</html>