<?php
        echo $this->Html->meta('icon');

         echo $this->Html->script('jquery-1.12.4.min');
echo $this->Html->script('plugins');
echo $this->Html->script('custom');
    ?>
<!DOCTYPE html>

<html lang="en" class="no-js">


<head>
  <!-- Basic need -->
  <title>Bach Ly</title>
  <meta charset="UTF-8">
  <meta name="description" content="ReaLand - Real Estate HTML Template">
  <meta name="keywords" content="">
  <meta name="author" content="">
  <link rel="profile" href="#">



</head>

<body>
<div id="page-loader">
    <div class="page-loader__logo">
        <img src="img/uploads/logo_dark.png" alt="ReaLand Logo">
    </div><!-- .page-loader__logo -->
    
    <div class="sk-folding-cube">
        <div class="sk-cube1 sk-cube"></div>
        <div class="sk-cube2 sk-cube"></div>
        <div class="sk-cube4 sk-cube"></div>
        <div class="sk-cube3 sk-cube"></div>
    </div><!-- .sk-folding-cube -->
</div><!-- .page-loader -->
<header class="header header--blue header--top">
  <div class="container">
    <div class="topbar">
      <ul class="topbar__contact">
        <li><span class="ion-ios-telephone-outline topbar__icon"></span><a href="tel:8801234567" class="topbar__link">(880)-123-4567</a></li>
        <li><span class="ion-ios-email-outline topbar__icon"></span><a href="mailto:ReaLand@support.com" class="topbar__link">ReaLand@support.com</a></li>
      </ul><!-- .topbar__contact -->

      <ul class="topbar__user">
        <span class="ion-ios-person-outline topbar__user-icon"></span>
        <?php echo $this->Html->link("Logout", array('controller' => 'users','action'=> 'logout'), array( 'class' => 'topbar__link'))?>
        
      </ul>
    </div><!-- .topbar -->

    <div class="header__main">
      <div class="header__logo">
        <a href="index-2.html">
          <h1 class="screen-reader-text">Realand</h1>
         <?php echo $this->Html->image('uploads/logo.png');?>
        </a>
      </div><!-- .header__main -->

      <div class="nav-mobile">
        <a href="#" class="nav-toggle">
          <span></span>
        </a><!-- .nav-toggle -->
      </div><!-- .nav-mobile -->

      <div class="header__menu header__menu--v1">
        <ul class="header__nav">
          <li class="header__nav-item">
          
            <?php echo $this->Html->link("Homes", array('controller' => 'Posts','action'=> 'index'), array( 'class' => 'header__nav-link'))?>
          </li>
         <li class="header__nav-item">
          
            <?php echo $this->Html->link("About Us", array('controller' => 'Homes','action'=> 'about'), array( 'class' => 'header__nav-link'))?>
          </li>
            
          </li>
          
          <li class="header__nav-item">
          
            <?php echo $this->Html->link("Ads", array('controller' => 'Posts','action'=> 'index'), array( 'class' => 'header__nav-link'))?>
          </li>
          <li class="header__nav-item">
            <a href="contact.html" class="header__nav-link">Contact Us</a>
            
          </li>
          
          
              
          
            
          <li class="header__nav-item">
          
            <?php echo $this->Html->link("Blog", array('controller' => 'Blogs','action'=> 'index'), array( 'class' => 'header__nav-link'))?>
          </li>
        </ul><!-- .header__nav -->

       
      </div><!-- .header__menu -->
<?php echo $this->Html->link("+  Submit Add", array('controller' => 'posts','action'=> 'add'), array( 'class' => 'header__cta'))?>
  
    </div><!-- .header__main -->
  </div><!-- .container -->
</header><!-- .header -->
<section style="margin-top: 200px;" class="submit-property">
    <div class="container">
      
           
        </ul><!-- .ht-breadcrumb -->

        <div class="submit-property__container">
            <div class="row">
                <div class="col-md-3">
                    <h2 class="bookmarked-listing__headline">Howdy, <strong>FelixDG!</strong></h2>
                    <div class="settings-block">
                        <span class="settings-block__title">Manage Account</span>
                        <ul class="settings">
                            <li class="setting"><a href="my_profile.html" class="setting__link"><i class="ion-ios-person-outline setting__icon"></i>My Profile</a></li>
                            <li class="setting"><a href="bookmarked_listing.html" class="setting__link"><i class="ion-ios-heart-outline setting__icon"></i>Bookmarked Listings</a></li>
                        </ul><!-- settings -->
                    </div><!-- .settings-block -->

                    <div class="settings-block">
                        <span class="settings-block__title">Manage Listing</span>
                        <ul class="settings">
                            <li class="setting"><a href="my_property.html" class="setting__link"><i class="ion-ios-home-outline setting__icon"></i>My Property</a></li>
                            <li class="setting setting--current"><a href="#" class="setting__link"><i class="ion-ios-upload-outline setting__icon"></i>Submit New Property</a></li>
                            <li class="setting"><a href="package.html" class="setting__link"><i class="ion-ios-cart-outline setting__icon"></i>My Packages</a></li>
                        </ul><!-- settings -->
                    </div><!-- .settings-block -->

                    <div class="settings-block">
                        <ul class="settings">
                            <li class="setting"><a href="change_password.html" class="setting__link"><i class="ion-ios-unlocked-outline setting__icon"></i>Change Password</a></li>
                            <li class="setting"><a href="index-2.html" class="setting__link"><i class="ion-ios-undo-outline setting__icon"></i>Log Out</a></li>
                        </ul><!-- settings -->
                    </div><!-- .settings-block -->
                </div><!-- .col -->
                <?php echo $this->Form->create('User'); ?>
                    <div class="col-md-4">
<?php

                    


		echo $this->Form->input('username',array('class' => 'my-profile__field'));


		?>

                        <label for="profile-first-name" class="my-profile__label" >First Name</label>
                        <input type="text"  class="my-profile__field"  name="username">

                        <label for="profile-last-name" class="my-profile__label">Last Name</label>
                        <input type="text" value="Haintheme" class="my-profile__field" id="profile-last-name">

                        <label for="profile-title" class="my-profile__label">Agent Title</label>
                        <input type="text" value="Haintheme" class="my-profile__field" id="profile-title">

                        <label for="profile-number" class="my-profile__label">Phone Number</label>
                        <input type="text" value="(222) 123-456" class="my-profile__field" id="profile-number">

                        <label for="profile-email" class="my-profile__label">Email</label>
                        <input type="email" value="haintheme@domain.com" class="my-profile__field" id="profile-email">

                        <label for="profile-twitter" class="my-profile__label">Twitter Address</label>
                        <input type="text" value="https://www.twitter.com/@haintheme" class="my-profile__field" id="profile-twitter">

                        <label for="profile-facebook" class="my-profile__label">Facebook Address</label>
                        <input type="text" value="https://www.facebook.com/haintheme" class="my-profile__field" id="profile-facebook">

                        <label for="profile-linkedin" class="my-profile__label">Linkedin Address</label>
                        <input type="text" value="https://www.linkedin.com/haintheme" class="my-profile__field" id="profile-linkedin">
                    </div><!-- .col -->
                        
                    <div class="col-md-5">
                        <label for="profile-introduce" class="my-profile__label">About Me</label>
                        <textarea id="profile-introduce" rows="20" class="my-profile__field">FelixDG is a handsome boy!</textarea>

                        <label for="profile-avatar" class="my-profile__label">Avatar</label>
                        <div class="my-profile__wrapper">
                            <input type="file">
                            <span><i class="ion-image"></i> Upload Avatar</span>
                        </div>

                        <button type="submit" class="my-profile__submit">Save Changes</button>
                    </div><!-- .col -->
                </form>
            </div><!-- .row -->
        </div><!-- .my-profile__container -->
    </div><!-- .container -->
</section><!-- .my-profile -->

<a href="#" class="back-to-top"><span class="ion-ios-arrow-up"></span></a>


<!-- JS Files -->
<script src="js/jquery-1.12.4.min.js"></script>
<script src="js/plugins.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDyCxHyc8z9gMA5IlipXpt0c33Ajzqix4"></script>
<script src="../../../../cdn.rawgit.com/googlemaps/v3-utility-library/master/infobox/src/infobox.js"></script>
<script src="js/custom.js"></script>
</body>

<!-- Mirrored from haintheme.com/demo/html/realand/my_profile.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 23 Apr 2019 11:51:42 GMT -->
</html>