

<style>
      html, body, #map-canvas {
        height: 100%;
        margin: 0px;
        padding: 0px
      }
      .controls {
        margin-top: 16px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
      }

    
    
      #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
      }

      #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }
}</style>

<?php
        echo $this->Html->meta('icon');

         echo $this->Html->script('jquery-1.12.4.min');
echo $this->Html->script('plugins');
echo $this->Html->script('custom');


    ?>



<?php echo $this->Form->create('User'); ?>

        <div class="sign-up__container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="sign-up__header">
                        <h1 class="sign-up__textcontent"><a href="#log-in" class="sign-up__tab is-active"><?php echo $this->Html->link(__('Login'), array('action' => 'login'));?></a> <span>or</span> <a href="#sign-up" class="sign-up__tab">Sign Up</a></h1>
                    </div><!-- .sign-up__header -->

                    <div class="sign-up__main">
                        <form action="" class="sign-up__form is-visible" id="log-in">



                         
                       



                 <label for="sign-up-name" class="sign-up__label">Name</label>
                            <input type="text" class="sign-up__field" id="sign-up-name" placeholder="Enter your full name" name="username">
                            <label for="sign-up-email" class="sign-up__label">Email</label>
                            <input type="email" class="sign-up__field" id="sign-up-email" placeholder="Your email goes here" name="email">

                            <label for="sign-up-password" class="sign-up__label">Password</label>
                            <input type="password" class="sign-up__field" id="sign-up-password" placeholder="******" name="password">

                           
<label for="sign-up-phone" class="sign-up__label">Phone</label>
                            <input type="text" class="sign-up__field" id="sign-up-phone" placeholder="Enter your Contact" name="phone">
                         
                            <label for="sign-up-facebook_url" class="sign-up__label">FaceBook Url</label>
                            <input type="text" class="sign-up__field" id="sign-up-facebook_url" placeholder="Enter your FaceBook Url" name="facebook_url">
                  


<input id="pac-input" class="controls" type="text" placeholder="Search Box">
    <div id="map-canvas" style="width: 700px;height: 500px; margin-right:380px;"></div>

    <label for="sign-up-adress" class="sign-up__label">Latitude</label>
                            <input type="text" class="sign-up__field"  placeholder="Enter Latitude"   id="latitute" name="lat">
                            <label for="sign-up-adress" class="sign-up__label">Longitude</label>
                            <input type="text" class="sign-up__field"  placeholder="Enter Longitude"   id="longitute" name="lng">
<label for="sign-up-adress" class="sign-up__label">Adress</label>
                            <input type="text" class="sign-up__field"  placeholder="Enter your Adress"   id="address" name="adress">
 <button type="submit" class="sign-up__submit">Sign Up</button>
                            </div>

                        </form><!-- .sign-up__form -->
                    </div>
                </div>
            </div><!-- .row -->
        </div><!-- .sign-up__container -->
    </div><!-- .container -->
</section><!-- .sign-up -->


<title>Places search box</title>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places">
    </script>
    <script>
// This example adds a search box to a map, using the Google Place Autocomplete
// feature. People can enter geographical searches. The search box will return a
// pick list containing a mix of places and predicted search terms.

function initialize() {

  var markers = [];
  var map = new google.maps.Map(document.getElementById('map-canvas'), {
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });

  var defaultBounds = new google.maps.LatLngBounds(
      new google.maps.LatLng(31.52037245807592,74.35871913680535),
      new google.maps.LatLng(31.52037245807592, 74.35871913680535));
  map.fitBounds(defaultBounds);

  // Create the search box and link it to the UI element.
  var input = /** @type {HTMLInputElement} */(
      document.getElementById('pac-input'));
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

  var searchBox = new google.maps.places.SearchBox(
    /** @type {HTMLInputElement} */(input));

  // [START region_getplaces]
  // Listen for the event fired when the user selects an item from the
  // pick list. Retrieve the matching places for that item.
  google.maps.event.addListener(searchBox, 'places_changed', function() {
    var places = searchBox.getPlaces();

    if (places.length == 0) {
      return;
    }
    for (var i = 0, marker; marker = markers[i]; i++) {
      marker.setMap(null);
    }

    // For each place, get the icon, place name, and location.
    markers = [];
    var bounds = new google.maps.LatLngBounds();
    for (var i = 0, place; place = places[i]; i++) {
      var image = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      };

      // Create a marker for each place.
      var marker = new google.maps.Marker({
        map: map,
        icon: image,
        title: place.name,
        position: place.geometry.location
      });

      markers.push(marker);

      bounds.extend(place.geometry.location);
    }

    map.fitBounds(bounds);
  });
  // [END region_getplaces]

  // Bias the SearchBox results towards places that are within the bounds of the
  // current map's viewport.
  google.maps.event.addListener(map, 'bounds_changed', function() {
    var bounds = map.getBounds();
    searchBox.setBounds(bounds);
  });
  //other//
  //
  google.maps.event.addListener(map, 'click', function(event) {
  placeMarker(event.latLng);
  });
 //////////////
  var marker;
  function placeMarker(location) {
  if(marker)
  { //on vérifie si le marqueur existe
    marker.setPosition(location); //on change sa position
  }else
  {
    marker = new google.maps.Marker({ //on créé le marqueur
    position: location, 
    map: map
    });
  }
  document.getElementById('latitute').value=location.lat();
  document.getElementById('longitute').value=location.lng();
  getAddress(location);
  }
  /////
    function getAddress(latLng)
  {
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode( {'latLng': latLng},
    function(results, status) {
    if(status == google.maps.GeocoderStatus.OK) {
      if(results[0])
      {
        document.getElementById("address").value = results[0].formatted_address;
      }
      else
      {
        document.getElementById("address").value = "No results";
      }
    }
    else 
    {
      document.getElementById("address").value = status;
    }
    });
  }
  
  //other//
}

google.maps.event.addDomListener(window, 'load', initialize);

    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBz61K4r9VDLjfEpFX5ntIsTA9hLlZ34ds&libraries=places&callback=initAutocomplete"
         async defer></script>
    </div>
