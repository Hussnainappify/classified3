

 

<html lang="en" class="no-js">

<!-- Mirrored from haintheme.com/demo/html/realand/single_property_1.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 23 Apr 2019 11:49:58 GMT -->
<head>
	<!-- Basic need -->
	<title>ReaLand</title>
	<meta charset="UTF-8">
	<meta name="description" content="ReaLand - Real Estate HTML Template">
	<meta name="keywords" content="">
	<meta name="author" content="">
	<link rel="profile" href="#">
	<!-- <link rel="shortcut icon" href="images/favicon.ico"> -->

	<!-- Mobile specific meta -->
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta name="format-detection" content="telephone-no">

	<!-- External Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600|Raleway:400,700,800|Roboto:400,500,700" rel="stylesheet"> 

	<!-- CSS files -->
<?php
        echo $this->Html->meta('icon');

         echo $this->Html->script('jquery-1.12.4.min');
echo $this->Html->script('plugins');
echo $this->Html->script('custom');
    ?>
</head>



<header class="header header--blue header--top">
  <div class="container">
    <div class="topbar">
      <ul class="topbar__contact">
        <li><span class="ion-ios-telephone-outline topbar__icon"></span><a href="tel:8801234567" class="topbar__link">(880)-123-4567</a></li>
        <li><span class="ion-ios-email-outline topbar__icon"></span><a href="mailto:ReaLand@support.com" class="topbar__link">ReaLand@support.com</a></li>
      </ul><!-- .topbar__contact -->

      <ul class="topbar__user">
        <span class="ion-ios-person-outline topbar__user-icon"></span>
        <?php echo $this->Html->link("Signin", array('controller' => 'users','action'=> 'login'), array( 'class' => 'topbar__link'))?>
        
      </ul>
    </div><!-- .topbar -->

    <div class="header__main">
      <div class="header__logo">
        <a href="index-2.html">
          <h1 class="screen-reader-text">Realand</h1>
           <?php echo $this->Html->image('uploads/logo.png');?>
         
        </a>
      </div><!-- .header__main -->

      <div class="nav-mobile">
        <a href="#" class="nav-toggle">
          <span></span>
        </a><!-- .nav-toggle -->
      </div><!-- .nav-mobile -->

      <div class="header__menu header__menu--v1">
        <ul class="header__nav">
          <li class="header__nav-item">
          
            <?php echo $this->Html->link("Homes", array('controller' => 'Posts','action'=> 'index'), array( 'class' => 'header__nav-link'))?>
          </li>
        <li class="header__nav-item">
          
            <?php echo $this->Html->link("About Us", array('controller' => 'Homes','action'=> 'about'), array( 'class' => 'header__nav-link'))?>
          </li>
            
          </li>
          
          <li class="header__nav-item">
          
            <?php echo $this->Html->link("Ads", array('controller' => 'Posts','action'=> 'index'), array( 'class' => 'header__nav-link'))?>
          </li>
          <li class="header__nav-item">
            <a href="contact.html" class="header__nav-link">Contact Us</a>
            
          </li>
          
          
              
          
            
          <li class="header__nav-item">
          
            <?php echo $this->Html->link("Blog", array('controller' => 'Blogs','action'=> 'index'), array( 'class' => 'header__nav-link'))?>
          </li>
        </ul><!-- .header__nav -->

        <ul class="topbar__user">
          <li><a href="sign_up.html" class="topbar__link">Sign In</a></li>
          <li><a href="sign_up.html" class="topbar__link">Join</a></li>
        </ul>

      </div><!-- .header__menu -->
<?php echo $this->Html->link("+  Submit Add", array('controller' => 'posts','action'=> 'add'), array( 'class' => 'header__cta'))?>
  
    </div><!-- .header__main -->
  </div><!-- .container -->

<body>

    </div><!-- .header__main -->
  </div><!-- .container -->
</header><!-- .header -->
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
<section class="property">
  




  

    
    <dt><?php echo __('LAT'); ?></dt>

    <dd id="latitute"><?php echo h($post['Post']['latitute']);?></dd>
    
    <dt><?php echo __('Long'); ?></dt>
    <dd>
      <dd id="longitute"><?php echo h($post['Post']['longitute']);?></dd>
    </dd>
    <dt><?php echo __('Address'); ?></dt>
    <dd>
      <?php echo h($post['Post']['adress']); ?>
      &nbsp;
    </dd>
    


  </dl>



  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDyCxHyc8z9gMA5IlipXpt0c33Ajzqix4"></script>
<script>
    var myMap;
    var latitute = document.getElementById('latitute').innerHTML;
    var longitute = document.getElementById('longitute').innerHTML;
    // alert(lat);
    var myLatlng = new google.maps.LatLng(latitute,longitute);
    function initialize() {
        var mapOptions = {
            zoom: 13,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false
        }
        myMap = new google.maps.Map(document.getElementById('map'), mapOptions);
        var marker = new google.maps.Marker({
            position: myLatlng,
            map: myMap,
          
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>

<div id="map" style="width:500px; height: 500px;margin-left: 400px;">

</div>
