

 

<html lang="en" class="no-js">

<!-- Mirrored from haintheme.com/demo/html/realand/single_property_1.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 23 Apr 2019 11:49:58 GMT -->
<head>
	<!-- Basic need -->
	<title>ReaLand</title>
	<meta charset="UTF-8">
	<meta name="description" content="ReaLand - Real Estate HTML Template">
	<meta name="keywords" content="">
	<meta name="author" content="">
	<link rel="profile" href="#">
	<!-- <link rel="shortcut icon" href="images/favicon.ico"> -->

	<!-- Mobile specific meta -->
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta name="format-detection" content="telephone-no">

	<!-- External Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600|Raleway:400,700,800|Roboto:400,500,700" rel="stylesheet"> 

	<!-- CSS files -->
<?php
        echo $this->Html->meta('icon');

         echo $this->Html->script('jquery-1.12.4.min');
echo $this->Html->script('plugins');
echo $this->Html->script('custom');
    ?>
</head>



<header class="header header--blue header--top">
  <div class="container">
    <div class="topbar">
      <ul class="topbar__contact">
        <li><span class="ion-ios-telephone-outline topbar__icon"></span><a href="tel:8801234567" class="topbar__link">(880)-123-4567</a></li>
        <li><span class="ion-ios-email-outline topbar__icon"></span><a href="mailto:ReaLand@support.com" class="topbar__link">ReaLand@support.com</a></li>
      </ul><!-- .topbar__contact -->

      <ul class="topbar__user">
        <span class="ion-ios-person-outline topbar__user-icon"></span>
        <?php echo $this->Html->link("Signin", array('controller' => 'users','action'=> 'login'), array( 'class' => 'topbar__link'))?>
        
      </ul>
    </div><!-- .topbar -->

    <div class="header__main">
      <div class="header__logo">
        <a href="index-2.html">
          <h1 class="screen-reader-text">Realand</h1>
           <?php echo $this->Html->image('uploads/logo.png');?>
         
        </a>
      </div><!-- .header__main -->

      <div class="nav-mobile">
        <a href="#" class="nav-toggle">
          <span></span>
        </a><!-- .nav-toggle -->
      </div><!-- .nav-mobile -->

      <div class="header__menu header__menu--v1">
        <ul class="header__nav">
          <li class="header__nav-item">
          
            <?php echo $this->Html->link("Homes", array('controller' => 'Posts','action'=> 'index'), array( 'class' => 'header__nav-link'))?>
          </li>
        <li class="header__nav-item">
          
            <?php echo $this->Html->link("About Us", array('controller' => 'Homes','action'=> 'about'), array( 'class' => 'header__nav-link'))?>
          </li>
            
          </li>
          
          <li class="header__nav-item">
          
            <?php echo $this->Html->link("Ads", array('controller' => 'Posts','action'=> 'index'), array( 'class' => 'header__nav-link'))?>
          </li>
          <li class="header__nav-item">
            <a href="contact.html" class="header__nav-link">Contact Us</a>
            
          </li>
          
          
              
          
            
          <li class="header__nav-item">
          
            <?php echo $this->Html->link("Blog", array('controller' => 'Blogs','action'=> 'index'), array( 'class' => 'header__nav-link'))?>
          </li>
        </ul><!-- .header__nav -->

        <ul class="topbar__user">
          <li><a href="sign_up.html" class="topbar__link">Sign In</a></li>
          <li><a href="sign_up.html" class="topbar__link">Join</a></li>
        </ul>

      </div><!-- .header__menu -->
<?php echo $this->Html->link("+  Submit Add", array('controller' => 'posts','action'=> 'add'), array( 'class' => 'header__cta'))?>
  
    </div><!-- .header__main -->
  </div><!-- .container -->

<body>

    </div><!-- .header__main -->
  </div><!-- .container -->
</header><!-- .header -->
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
<section class="property">
  

  <div class="property__header">
    <div class="container">
      <div class="property__header-container">
        <ul class="property__main">
            <li class="property__title property__main-item">
            <div class="property__meta">
              <span class="property__offer">For Sale</span>
             
            </div><!-- .property__meta -->
            <p class="property__name"><?php echo h($post['Post']['title']); ?></p>
           
          </li>
          <li class="property__title property__main-item">
            <div class="property__meta">
              <span class="property__offer">Condation</span>
             
            </div><!-- .property__meta -->
            <p class="property__name"><?php echo h($post['Post']['status']); ?></p>
           
          </li>

          <li class="property__title property__main-item">
            <div class="property__meta">
              <span class="property__offer">AD ID</span>
             
            </div><!-- .property__meta -->
            <p class="property__name"><?php echo h($post['Post']['id']); ?></p>
           
          </li>
         <li class="property__title property__main-item">
            <div class="property__meta">
              <span class="property__offer">Price</span>
             
            </div><!-- .property__meta -->
            <p class="property__name"><?php echo h($post['Post']['price']); ?></p>
           
          </li>
          <li class="property__title property__main-item">
            <div class="property__meta">
              <span class="property__offer">Views</span>
             
            </div><!-- .property__meta -->
            <p class="property__name"><h2><div id="CounterVisitor"></h2></p>
           
          </li>
            </ul><!-- .listing__stats -->
          </li>

        </ul><!-- .property__main -->

        <ul class="property__list">
          <li class="property__item">
            <a href="#" class="property__link">
              <i class="fa fa-heart-o property__icon" aria-hidden="true"></i>  
              <span class="property__item-desc">Favorite</span>
            </a>
          </li>
          <li class="property__item">
            <a href="#" class="property__link">
              <i class="ion-ios-loop-strong property__icon"></i>
              <span class="property__item-desc">Compare</span>
            </a>
          </li>
          <li class="property__item">
            <a href="#" class="property__link">
              <i class="ion-android-share-alt property__icon"></i>
              <span class="property__item-desc">Share</span>
            </a>
          </li>
        </ul><!-- .property__list -->
      </div><!-- .property__header-container -->
    </div><!-- .container -->
  </div><!-- .property__header -->

     
     <div class="property__slider">
    <div class="container">
      <div class="property__slider-container property__slider-container--vertical">
        <ul class="property__slider-nav property__slider-nav--vertical">
          <li class="property__slider-item">
             <?php if($post['Post']['images'] != "") { ?>
                    <img src="../../uploadjh/<?php echo $post['Post']['images']; ?>" >
                    <?php } ?>
          </li>

          <li class="property__slider-item">
             <?php if($post['Post']['images'] != "") { ?>
                    <img src="../../uploadjh/<?php echo $post['Post']['back_image']; ?>" >
                    <?php } ?>
          </li>

          <li class="property__slider-item">
            <?php if($post['Post']['images'] != "") { ?>
                    <img src="../../uploadjh/<?php echo $post['Post']['side_image']; ?>" >
                    <?php } ?>
          </li>

          <li class="property__slider-item">
            <?php if($post['Post']['images'] != "") { ?>
                    <img src="../../uploadjh/<?php echo $post['Post']['side_image2']; ?>" >
                    <?php } ?>
          </li>
       


       </ul>
         

        <div class="property__slider-main property__slider-main--vertical">
          <div class="property__slider-images">
       

         
            <div class="property__slider-image">
             <?php if($post['Post']['images'] != "") { ?>
                    <img src="../../uploadjh/<?php echo $post['Post']['images']; ?>"  style="width:700px;height:400px;border-color:red;margin-top:70px;margin-left: 120px;" >
                    <?php } ?>
            </div><!-- .property__slider-image -->

            <div class="property__slider-image">
           <?php if($post['Post']['images'] != "") { ?>
                    <img src="../../uploadjh/<?php echo $post['Post']['back_image']; ?>"  style="width:700px;height:400px;border-color:red;margin-top:70px;margin-left: 120px;" >
                    <?php } ?>
            </div><!-- .property__slider-image -->

            <div class="property__slider-image">
            <?php if($post['Post']['images'] != "") { ?>
                    <img src="../../uploadjh/<?php echo $post['Post']['side_image']; ?>"  style="width:700px;height:400px;border-color:red;margin-top:70px;margin-left: 120px;" >
                    <?php } ?>
            </div><!-- .property__slider-image -->

            <div class="property__slider-image">
           <?php if($post['Post']['images'] != "") { ?>
                    <img src="../../uploadjh/<?php echo $post['Post']['side_image2']; ?>"  style="width:700px;height:400px;border-color:red;margin-top:70px;margin-left: 120px;" >
                    <?php } ?>
            </div><!-- .property__slider-image -->
          </div><!-- .property__slider-images -->

          <ul class="image-navigation">
            <li class="image-navigation__prev">
              <span class="ion-ios-arrow-left"></span>
            </li>
            <li class="image-navigation__next">
              <span class="ion-ios-arrow-right"></span>
            </li>
          </ul>

          <span class="property__slider-info"><i class="ion-android-camera"></i><span class="sliderInfo"></span></span>
        </div><!-- .property__slider-main -->
      </div><!-- .property__slider-container -->
    </div><!-- .container -->
  </div><!-- .property__slider -->
          

  
 
  <div class="property__container">
    <div class="container">
      <div class="row">
        <aside class="col-md-3">
          <div class="widget__container">
            <section class="widget">
              <form class="contact-form contact-form--white">
                <div class="contact-form__header">
                  <div class="contact-form__header-container">
                    <?php echo $this->Html->image('uploads/contact_agent.png');?>
                 
                    <div class="contact-info">
                    <span class="contact-company">Contact With Seller</span>
               
                      <a  class="contact-number">tel:+<?php echo h($post['Post']['contact']); ?></a>
                    </div><!-- .contact-info -->
                  </div><!-- .contact-form__header-container -->
                </div><!-- .contact-form__header -->
                
                <div class="contact-form__body">
                  <input type="text" class="contact-form__field" placeholder="Name" name="name" required>
                  <input type="email" class="contact-form__field" placeholder="Email" name="email" required>
                  <input type="tel" class="contact-form__field" placeholder="Phone number" name="contact">
                  <textarea class="contact-form__field contact-form__comment" cols="30" rows="5" placeholder="Comment" name="comment" required></textarea>
                </div><!-- .contact-form__body -->

                <div class="contact-form__footer">
                  <input type="submit" class="contact-form__submit" name="submit" value="Contact Agent">
                </div><!-- .contact-form__footer -->
              </form><!-- .contact-form -->
            </section><!-- .widget -->

           
             
        
        </aside>
        
        <main class="col-md-9">
          <div class="property__feature-container">
            <div class="property__feature">
              <h3 class="property__feature-title">Open House</h3>
              <div class="property__feature-schedule">
                <ul class="property__feature-time-list">
            
                  <?php echo h($post['Post']['adress']); ?>
                </ul>
            
              </div><!-- .property__feature-schedule -->
          
            </div><!-- .property__feature -->

            <div class="property__feature">
              <h3 class="property__feature-title property__feature-title--b-spacing">Property Description</h3>
              <p><?php echo h($post['Post']['description']); ?></p>

             
            </div><!-- .property__feature -->

            <?php echo $this->Form->create('Message'); ?>
              
            <div class="property__feature">
              <h3 class="property__feature-title property__feature-title--b-spacing">Schedule A Visit</h3>
              <form class="property__form">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="property__form-wrapper">
                      <input type="text" name="date" class="property__form-field property__form-date" placeholder="Monday" data-format="l, F d, Y"  data-min-year="2017"  data-max-year="2020" data-translate-mode="true" data-modal="true" data-large-mode="true" required>
                      <span class="ion-android-calendar property__form-icon"></span>
                    </div><!-- .property__form-wrapper -->
                  </div><!-- col -->
                  <div class="col-sm-6">
                    <div class="property__form-wrapper">
                      <input type="text" name="time" class="property__form-field property__form-time" placeholder="07:30 AM" required>
                      <span class="ion-android-alarm-clock property__form-icon"></span>
                    </div><!-- .property__form-wrapper -->
                  </div><!-- col -->
                  <div class="col-sm-6">
                    <input type="text" name="name" class="property__form-field" placeholder="Your Name" required>
                  </div><!-- col -->
                  <div class="col-sm-6">
                    <input type="text" name="contact" class="property__form-field" placeholder="Your Email/Phone" required>
                  </div><!-- col -->
                  <div class="col-sm-12">
                    <textarea rows="4" name="message" class="property__form-field" placeholder="Message" required></textarea>
                  </div><!-- col -->
                  <div class="col-sm-12">
                    <input name="submit" type="submit" class="property__form-submit" value="Submit"></input>
                  </div><!-- .col -->
                </div><!-- .row -->
              </form>
            </div><!-- .property__feature -->
          </div><!-- .property__feature-container -->
        </main>
      </div><!-- .row -->
    </div><!-- .container -->
  </div><!-- .property__container -->
</section><!-- .property -->



<a href="#" class="back-to-top"><span class="ion-ios-arrow-up"></span></a>


</body>

<!-- Mirrored from haintheme.com/demo/html/realand/single_property_1.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 23 Apr 2019 11:51:41 GMT -->






 <script>
var n = localStorage.getItem('on_load_counter');

if (n === null) {
  n = 0;
}
n++;

localStorage.setItem("on_load_counter", n);

nums = n.toString().split('').map(Number);
document.getElementById('CounterVisitor').innerHTML = '';
for (var i of nums) {
  document.getElementById('CounterVisitor').innerHTML += '<span class="counter-item">' + i + '</span>';
}

</script>