<?php
        echo $this->Html->meta('icon');

         echo $this->Html->script('jquery-1.12.4.min');
echo $this->Html->script('plugins');
echo $this->Html->script('custom');
    ?>
<!DOCTYPE html>

<html lang="en" class="no-js">


<head>
  <!-- Basic need -->
  <title>Bach Ly</title>
  <meta charset="UTF-8">
  <meta name="description" content="ReaLand - Real Estate HTML Template">
  <meta name="keywords" content="">
  <meta name="author" content="">
  <link rel="profile" href="#">



</head>

<body>
<div id="page-loader">
    <div class="page-loader__logo">
        <img src="img/uploads/logo_dark.png" alt="ReaLand Logo">
    </div><!-- .page-loader__logo -->
    
    <div class="sk-folding-cube">
        <div class="sk-cube1 sk-cube"></div>
        <div class="sk-cube2 sk-cube"></div>
        <div class="sk-cube4 sk-cube"></div>
        <div class="sk-cube3 sk-cube"></div>
    </div><!-- .sk-folding-cube -->
</div><!-- .page-loader -->
<header class="header header--blue header--top">
  <div class="container">
    <div class="topbar">
      <ul class="topbar__contact">
        <li><span class="ion-ios-telephone-outline topbar__icon"></span><a href="tel:8801234567" class="topbar__link">(880)-123-4567</a></li>
        <li><span class="ion-ios-email-outline topbar__icon"></span><a href="mailto:ReaLand@support.com" class="topbar__link">ReaLand@support.com</a></li>
      </ul><!-- .topbar__contact -->

     
    </div><!-- .topbar -->

    <div class="header__main">
      <div class="header__logo">
        <a href="index-2.html">
          <h1 class="screen-reader-text">Realand</h1>
         <?php echo $this->Html->image('uploads/logo.png');?>
        </a>
      </div><!-- .header__main -->

      <div class="nav-mobile">
        <a href="#" class="nav-toggle">
          <span></span>
        </a><!-- .nav-toggle -->
      </div><!-- .nav-mobile -->

      <div class="header__menu header__menu--v1">
        <ul class="header__nav">
          <li class="header__nav-item">
          
            <?php echo $this->Html->link("Homes", array('controller' => 'Posts','action'=> 'index'), array( 'class' => 'header__nav-link'))?>
          </li>
         <li class="header__nav-item">
          
            <?php echo $this->Html->link("About Us", array('controller' => 'Homes','action'=> 'about'), array( 'class' => 'header__nav-link'))?>
          </li>
            
          </li>
          
          <li class="header__nav-item">
          
            <?php echo $this->Html->link("Ads", array('controller' => 'Posts','action'=> 'index'), array( 'class' => 'header__nav-link'))?>
          </li>
          <li class="header__nav-item">
            <a href="contact.html" class="header__nav-link">Contact Us</a>
            
          </li>
          
          
              
          
            
          <li class="header__nav-item">
          
            <?php echo $this->Html->link("Blog", array('controller' => 'Blogs','action'=> 'index'), array( 'class' => 'header__nav-link'))?>
          </li>
        </ul><!-- .header__nav -->

       
      </div><!-- .header__menu -->
<?php echo $this->Html->link("+  Submit Add", array('controller' => 'posts','action'=> 'add'), array( 'class' => 'header__cta'))?>
  
    </div><!-- .header__main -->
  </div><!-- .container -->
</header><!-- .header -->
<section style="margin-top: 200px;" class="submit-property">
    <div class="container">
      
           
        </ul><!-- .ht-breadcrumb -->

        <div class="submit-property__container">
            <div class="row">
                <div class="col-md-3">
                    <h2 class="bookmarked-listing__headline">Hy   <strong><?php echo $this->Session->read('Auth.User.username') ; ?></strong></h2>
                    <div class="settings-block">
                        <span class="settings-block__title">Manage Account</span>
                        <ul class="settings">
                            <li class="setting"></i><?php echo $this->Html->link("My Profile", array('controller' => 'Users','action'=> 'profile'),array( 'class' => 'setting__link'))?><i class="ion-ios-person-outline setting__icon"></i></a></li>
                            <li class="setting"><a href="bookmarked_listing.html" class="setting__link"><i class="ion-ios-heart-outline setting__icon"></i>Bookmarked Listings</a></li>
                        </ul><!-- settings -->
                    </div><!-- .settings-block -->

                    <div class="settings-block">
                        <span class="settings-block__title">Manage Listing</span>
                        <ul class="settings">
                            <li class="setting"><?php echo $this->Html->link("My Ads", array('controller' => 'posts','action'=> 'myadd'),array( 'class' => 'setting__link'))?><i class="ion-ios-home-outline setting__icon"></i></a></li>
                            <li class="setting setting--current"><?php echo $this->Html->link("Submit New Property", array('controller' => 'posts','action'=> 'add'),array( 'class' => 'setting__link'))?><i class="ion-ios-upload-outline setting__icon"></i></a></li>
                          
                        </ul><!-- settings -->
                    </div><!-- .settings-block -->

                    <div class="settings-block">
                        <ul class="settings">
                            <li class="setting"> <?php echo $this->Html->link("Logout", array('controller' => 'users','action'=> 'logout'),array( 'class' => 'setting__link'))?><i class="ion-ios-undo-outline setting__icon"></i></a></li>
                          
                        </ul><!-- settings -->
                    </div><!-- .settings-block -->
                </div><!-- .col -->

               <?php echo $this->Form ->create('Post', array('type' => 'file')); ?>
           
                    <div class="col-md-9">
                        <div class="submit-property__block">
                            <h3 class="submit-property__headline">Basic Information</h3>
                 
                            <div class="submit-property__group">
                                <label for="property-title" class="submit-property__label">Title *</label>
                                <input type="text" class="submit-property__field" id="property-title" placeholder="Perfect  for Sale" required name="title">
                            </div><!-- .submit-property__group -->

                           

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="submit-property__group">
                                        <label for="property-price" class="submit-property__label">Property Price *</label>
                                        <input type="text" class="submit-property__field" id="property-price" required name="price">
                                        <span class="submit-property__unit">USD</span>
                                    </div><!-- .submit-property__group -->
                                </div><!-- .col -->

                                <div class="col-md-4">
                                    <div class="submit-property__group">
                                        <label for="property-area" class="submit-property__label">Condition</label>
                                        <input type="text" class="submit-property__field" id="property-area" required placeholder="New/Used" name="status">
                                       
                                    </div><!-- .submit-property__group -->
                                </div><!-- .col -->


<div class="col-md-4">
                                    <div class="submit-property__group">
                                        <label for="property-area" class="submit-property__label">Phone#</label>
                                        <input type="text" class="submit-property__field"  required placeholder="Contact" name="contact">
                                       
                                    </div><!-- .submit-property__group -->
                                </div><!-- .col -->

                                 <div class="col-md-4">
                                    <div class="submit-property__group">
                                        <label for="property-area" class="submit-property__label">Total#</label>
                                        <input type="text" class="submit-property__field" id="property-area" required placeholder="Number of Items" name="qty">
                                       
                                    </div><!-- .submit-property__group -->
                                </div><!-- .col -->

                                
                            </div><!-- .row -->
  <label for="property-area" class="submit-property__label">*Description</label>
                          <textarea name="description" rows="10" cols="30" style="border-color:green;"></textarea>

                            <div class="submit-property__group">
                               
                            </div><!-- .submit-property__group -->
                        </div><!-- .submit-property__block -->

                        <div class="submit-property__block">
                            <h3 class="submit-property__headline">Gallery</h3>
                            
    <div class="row">
                                <div class="col-md-5">
                                    <div class="submit-property__group">
                                 
                                        <div class="submit-property__upload">
                                            <input type="file" name="images" id='img' onchange="validateImage()" >
                                            <div class="submit-property__upload-inner">
                                                <span class="ion-ios-plus-outline submit-property__icon"></span>
                                                <span class="submit-property__upload-desc">Drop image here or click to upload</span>
                                            </div>
                                        </div><!-- .submit-proeprty__upload -->
                                    </div><!-- .submit-property__group -->
                                </div><!-- .col -->

                                 <div class="col-md-5">
                                    <div class="submit-property__group">
                                
                                        <div class="submit-property__upload">
                                            <input type="file" name="back_image" id='img' onchange="validateImage()" >
                                            <div class="submit-property__upload-inner">
                                                <span class="ion-ios-plus-outline submit-property__icon"></span>
                                                <span class="submit-property__upload-desc">Drop image here or click to upload</span>
                                            </div>
                                        </div><!-- .submit-proeprty__upload -->
                                    </div><!-- .submit-property__group -->
                                </div><!-- .col -->


                              <div class="col-md-5">
                                    <div class="submit-property__group">
                              
                                        <div class="submit-property__upload">
                                            <input type="file" name="side_image" id='img' onchange="validateImage()" >
                                            <div class="submit-property__upload-inner">
                                                <span class="ion-ios-plus-outline submit-property__icon"></span>
                                                <span class="submit-property__upload-desc">Drop image here or click to upload</span>
                                            </div>
                                        </div><!-- .submit-proeprty__upload -->
                                    </div><!-- .submit-property__group -->
                                </div><!-- .col -->
                           <div class="col-md-5">
                                    <div class="submit-property__group">
                                    
                                        <div class="submit-property__upload">
                                            <input type="file" name="side_image2" id='img' onchange="validateImage()" >
                                            <div class="submit-property__upload-inner">
                                                <span class="ion-ios-plus-outline submit-property__icon"></span>
                                                <span class="submit-property__upload-desc">Drop image here or click to upload</span>
                                            </div>
                                        </div><!-- .submit-proeprty__upload -->
                                    </div><!-- .submit-property__group -->
                                </div><!-- .col -->
                               
                      

<input id="pac-input" class="controls" type="text" placeholder="Search Box">
    <div id="map-canvas" style="width: 700px;height: 500px; margin-right:380px;"></div>

    <label for="sign-up-adress" class="sign-up__label">Latitude</label>
                            <input type="text" class="sign-up__field"  placeholder="Enter Latitude"   id="latitute" name="latitute">
                            <label for="sign-up-adress" class="sign-up__label">Longitude</label>
                            <input type="text" class="sign-up__field"  placeholder="Enter Longitude"   id="longitute" name="longitute">
<label for="sign-up-adress" class="sign-up__label">Adress</label>
                            <input type="text" class="sign-up__field"  placeholder="Enter your Adress"   id="address" name="adress">
                                
                                    </div><!-- .submit-property__group -->
                                </div><!-- .col -->
                            </div><!-- .row -->
                        </div><!-- .submit-property__block -->

                  

                       

                        <button type="submit" class="submit-property__submit" style="margin-left:370px;"  >Submit</button>
                    </div><!-- .col -->
                </form>
        
            </div><!-- .row -->
        </div><!-- .submit-property__container -->
    </div><!-- .container -->
</section><!-- .submit-property -->


<a href="#" class="back-to-top"><span class="ion-ios-arrow-up"></span></a>
<div id="submit-property-wysiwyg-icons">
 
</div>

<!-- JS Files -->
<script src="js/jquery-1.12.4.min.js"></script>
<script src="js/plugins.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDyCxHyc8z9gMA5IlipXpt0c33Ajzqix4"></script>
<script src="../../../../cdn.rawgit.com/googlemaps/v3-utility-library/master/infobox/src/infobox.js"></script>
<script src="js/custom.js"></script>
</body>

</html>
<title>Places search box</title>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places">
    </script>
    <script>
// This example adds a search box to a map, using the Google Place Autocomplete
// feature. People can enter geographical searches. The search box will return a
// pick list containing a mix of places and predicted search terms.

function initialize() {

  var markers = [];
  var map = new google.maps.Map(document.getElementById('map-canvas'), {
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });

  var defaultBounds = new google.maps.LatLngBounds(
      new google.maps.LatLng(31.52037245807592,74.35871913680535),
      new google.maps.LatLng(31.52037245807592, 74.35871913680535));
  map.fitBounds(defaultBounds);

  // Create the search box and link it to the UI element.
  var input = /** @type {HTMLInputElement} */(
      document.getElementById('pac-input'));
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

  var searchBox = new google.maps.places.SearchBox(
    /** @type {HTMLInputElement} */(input));

  // [START region_getplaces]
  // Listen for the event fired when the user selects an item from the
  // pick list. Retrieve the matching places for that item.
  google.maps.event.addListener(searchBox, 'places_changed', function() {
    var places = searchBox.getPlaces();

    if (places.length == 0) {
      return;
    }
    for (var i = 0, marker; marker = markers[i]; i++) {
      marker.setMap(null);
    }

    // For each place, get the icon, place name, and location.
    markers = [];
    var bounds = new google.maps.LatLngBounds();
    for (var i = 0, place; place = places[i]; i++) {
      var image = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      };

      // Create a marker for each place.
      var marker = new google.maps.Marker({
        map: map,
        icon: image,
        title: place.name,
        position: place.geometry.location
      });

      markers.push(marker);

      bounds.extend(place.geometry.location);
    }

    map.fitBounds(bounds);
  });
  // [END region_getplaces]

  // Bias the SearchBox results towards places that are within the bounds of the
  // current map's viewport.
  google.maps.event.addListener(map, 'bounds_changed', function() {
    var bounds = map.getBounds();
    searchBox.setBounds(bounds);
  });
  //other//
  //
  google.maps.event.addListener(map, 'click', function(event) {
  placeMarker(event.latLng);
  });
 //////////////
  var marker;
  function placeMarker(location) {
  if(marker)
  { //on vérifie si le marqueur existe
    marker.setPosition(location); //on change sa position
  }else
  {
    marker = new google.maps.Marker({ //on créé le marqueur
    position: location, 
    map: map
    });
  }
  document.getElementById('latitute').value=location.lat();
  document.getElementById('longitute').value=location.lng();
  getAddress(location);
  }
  /////
    function getAddress(latLng)
  {
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode( {'latLng': latLng},
    function(results, status) {
    if(status == google.maps.GeocoderStatus.OK) {
      if(results[0])
      {
        document.getElementById("address").value = results[0].formatted_address;
      }
      else
      {
        document.getElementById("address").value = "No results";
      }
    }
    else 
    {
      document.getElementById("address").value = status;
    }
    });
  }
  
  //other//
}

google.maps.event.addDomListener(window, 'load', initialize);

    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBz61K4r9VDLjfEpFX5ntIsTA9hLlZ34ds&libraries=places&callback=initAutocomplete"
         async defer></script>
    </div>

    </div>
<a href="#" class="back-to-top"><span class="ion-ios-arrow-up"></span></a>

<script >
   function validateImage() {
    var formData = new FormData();
 
    var file = document.getElementById("img").files[0];
 
    formData.append("Filedata", file);
    var t = file.type.split('/').pop().toLowerCase();
    if (t != "jpeg" && t != "jpg" && t != "png" && t != "bmp" && t != "gif") {
        alert('Please select a valid image file');
        document.getElementById("img").value = '';
        return false;
    }
    if (file.size > 1024000) {
        alert('Max Upload size is 1MB only');
        document.getElementById("img").value = '';
        return false;
    }
    return true;
}
</script>
</script>
