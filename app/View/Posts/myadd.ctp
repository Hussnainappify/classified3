<?php
        echo $this->Html->meta('icon');

         echo $this->Html->script('jquery-1.12.4.min');
echo $this->Html->script('plugins');
echo $this->Html->script('custom');
    ?>
<!DOCTYPE html>

<html lang="en" class="no-js">


<head>
  <!-- Basic need -->
  <title>Bach Ly</title>
  <meta charset="UTF-8">
  <meta name="description" content="ReaLand - Real Estate HTML Template">
  <meta name="keywords" content="">
  <meta name="author" content="">
  <link rel="profile" href="#">



</head>

<body>
<div id="page-loader">
    <div class="page-loader__logo">
        <img src="img/uploads/logo_dark.png" alt="ReaLand Logo">
    </div><!-- .page-loader__logo -->
    
    <div class="sk-folding-cube">
        <div class="sk-cube1 sk-cube"></div>
        <div class="sk-cube2 sk-cube"></div>
        <div class="sk-cube4 sk-cube"></div>
        <div class="sk-cube3 sk-cube"></div>
    </div><!-- .sk-folding-cube -->
</div><!-- .page-loader -->
<header class="header header--blue header--top">
  <div class="container">
    <div class="topbar">
      <ul class="topbar__contact">
        <li><span class="ion-ios-telephone-outline topbar__icon"></span><a href="tel:8801234567" class="topbar__link">(880)-123-4567</a></li>
        <li><span class="ion-ios-email-outline topbar__icon"></span><a href="mailto:ReaLand@support.com" class="topbar__link">classified@support.com</a></li>
      </ul><!-- .topbar__contact -->

     
    </div><!-- .topbar -->

    <div class="header__main">
      <div class="header__logo">
        <a href="index-2.html">
          <h1 class="screen-reader-text">Realand</h1>
         <?php echo $this->Html->image('uploads/logo.png');?>
        </a>
      </div><!-- .header__main -->

      <div class="nav-mobile">
        <a href="#" class="nav-toggle">
          <span></span>
        </a><!-- .nav-toggle -->
      </div><!-- .nav-mobile -->

      <div class="header__menu header__menu--v1">
        <ul class="header__nav">
          <li class="header__nav-item">
          
            <?php echo $this->Html->link("Homes", array('controller' => 'Posts','action'=> 'index'), array( 'class' => 'header__nav-link'))?>
          </li>
         <li class="header__nav-item">
          
            <?php echo $this->Html->link("About Us", array('controller' => 'Homes','action'=> 'about'), array( 'class' => 'header__nav-link'))?>
          </li>
            
          </li>
          
          <li class="header__nav-item">
          
            <?php echo $this->Html->link("Ads", array('controller' => 'Posts','action'=> 'index'), array( 'class' => 'header__nav-link'))?>
          </li>
          <li class="header__nav-item">
            <a href="contact.html" class="header__nav-link">Contact Us</a>
            
          </li>
          
          
              
          
            
          <li class="header__nav-item">
          
            <?php echo $this->Html->link("Blog", array('controller' => 'Blogs','action'=> 'index'), array( 'class' => 'header__nav-link'))?>
          </li>
        </ul><!-- .header__nav -->

       
      </div><!-- .header__menu -->
<?php echo $this->Html->link("+  Submit Add", array('controller' => 'posts','action'=> 'add'), array( 'class' => 'header__cta'))?>
  
    </div><!-- .header__main -->
  </div><!-- .container -->
</header><!-- .header -->
<section style="margin-top: 200px;" class="submit-property">
    <div class="container">
      
           
        </ul><!-- .ht-breadcrumb -->

        <div class="submit-property__container">
            <div class="row">
                <div class="col-md-3">
                    <h2 class="bookmarked-listing__headline"><b>Welcomeback</b>  <?php echo $this->Session->read('Auth.User.username') ; ?></h2>
                    <div class="settings-block">
                        <span class="settings-block__title">Manage Account</span>
                        <ul class="settings">
                         <li class="setting"></i><?php echo $this->Html->link("My Profile", array('controller' => 'Users','action'=> 'profile'),array( 'class' => 'setting__link'))?><i class="ion-ios-person-outline setting__icon"></i></a></li>
                            <li class="setting"><a href="bookmarked_listing.html" class="setting__link"><i class="ion-ios-heart-outline setting__icon"></i>Bookmarked Listings</a></li>
                        </ul><!-- settings -->
                    </div><!-- .settings-block -->

                    <div class="settings-block">
                        <span class="settings-block__title">Manage Listing</span>
                        <ul class="settings">
                            <li class="setting"><?php echo $this->Html->link("My Ads", array('controller' => 'posts','action'=> 'myadd'),array( 'class' => 'setting__link'))?><i class="ion-ios-home-outline setting__icon"></i></a></li>
                             <li class="setting setting--current"><?php echo $this->Html->link("Submit New Property", array('controller' => 'posts','action'=> 'add'),array( 'class' => 'setting__link'))?><i class="ion-ios-upload-outline setting__icon"></i></a></li>
                         
                        </ul><!-- settings -->
                    </div><!-- .settings-block -->

                    <div class="settings-block">
                        <ul class="settings">
                            <li class="setting"></i><?php echo $this->Html->link("Change Password", array('controller' => 'Users','action'=> 'Password'),array( 'class' => 'setting__link'))?><i class="ion-ios-unlocked-outline setting__icon"></i></a></li>
                            <li class="setting"> <?php echo $this->Html->link("Logout", array('controller' => 'users','action'=> 'logout'),array( 'class' => 'setting__link'))?><i class="ion-ios-undo-outline setting__icon"></i></a></li>
                        </ul><!-- settings -->
                    </div><!-- .settings-block -->
                </div><!-- .col -->
                
                   <div class="col-md-9">
                    <ul class="manage-list manage-list--my-property">
                        <li class="manage-list__header">
                            <span class="manage-list__title"><i class="fa fa-bookmark-o" aria-hidden="true"></i> My Property</span>
                     
                        </li>
                          <?php foreach ($posts as $post): 
                          	if($post['Post']['user_id'] == $this->Session->read('Auth.User.id')){?>
                        <li class="manage-list__item">
                            <div class="manage-list__item-container">
                                <div class="manage-list__item-img">
                                 
                                      <?php if($post['Post']['images'] != "") { ?>
                    <img src="../uploadjh/<?php echo $post['Post']['images']; ?>" class="listing__img" style="max-height: 200px; max-width: 200px;">
                   
                    <?php } ?>
                                   
                                    </a>
                                </div><!-- manage-list__item-img -->
                        
                                <div class="manage-list__item-detail">
                          
                                   <h4 class="listing__title"><?php echo h($post['Post']['title']); ?></a></h4>
                                    <p class="listing__location"><span class="ion-ios-location-outline listing__location-icon"></span><?php echo h($post['Post']['adress']); ?> </p>
                                    <p class="listing__price">$<?php echo h($post['Post']['price']); ?></p>
                                 
                                </div>
                            </div><!-- .manage-list__item-container -->

                          

                            <div class="manage-list__action">
                                <a  class="fa fa-pencil"<?php echo $this->Html->link(__('Edit'), array('action' => 'edit',$post['Post']['id']));?>
                                 <?php
                echo $this->Form->postLink(
                    'Delete',
                    array('action' => 'delete', $post['Post']['id']),array('class' =>'fa fa-times'),
                    array('confirm' => 'Are you sure?')
                );
            ?>
             
               
                               <a  class="fa fa-eye"<?php echo $this->Html->link(__('View'), array('action' => 'view',$post['Post']['id']));?>
                            </div>
                        </li><!-- .manage-list__item" -->

                       
                    </ul>
                     <?php }
                 endforeach; ?>
                </div><!-- .col -->

            </div><!-- .row -->
        </div><!-- .my-property__container -->
    </div><!-- .container -->
</section><!-- my-property -->





</html>