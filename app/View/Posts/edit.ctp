
<?php
        echo $this->Html->meta('icon');

         echo $this->Html->script('jquery-1.12.4.min');
echo $this->Html->script('plugins');
echo $this->Html->script('custom');
    ?>
<!DOCTYPE html>

<html lang="en" class="no-js">


<head>
  <!-- Basic need -->
  <title>Bach Ly</title>
  <meta charset="UTF-8">
  <meta name="description" content="ReaLand - Real Estate HTML Template">
  <meta name="keywords" content="">
  <meta name="author" content="">
  <link rel="profile" href="#">



</head>

<body>
<div id="page-loader">
    <div class="page-loader__logo">
        <img src="img/uploads/logo_dark.png" alt="ReaLand Logo">
    </div><!-- .page-loader__logo -->
    
    <div class="sk-folding-cube">
        <div class="sk-cube1 sk-cube"></div>
        <div class="sk-cube2 sk-cube"></div>
        <div class="sk-cube4 sk-cube"></div>
        <div class="sk-cube3 sk-cube"></div>
    </div><!-- .sk-folding-cube -->
</div><!-- .page-loader -->
<header class="header header--blue header--top">
  <div class="container">
    <div class="topbar">
      <ul class="topbar__contact">
        <li><span class="ion-ios-telephone-outline topbar__icon"></span><a href="tel:8801234567" class="topbar__link">(880)-123-4567</a></li>
        <li><span class="ion-ios-email-outline topbar__icon"></span><a href="mailto:ReaLand@support.com" class="topbar__link">ReaLand@support.com</a></li>
      </ul><!-- .topbar__contact -->

      <ul class="topbar__user">
        <span class="ion-ios-person-outline topbar__user-icon"></span>
        <?php echo $this->Html->link("Logout", array('controller' => 'users','action'=> 'logout'), array( 'class' => 'topbar__link'))?>
      
      </ul>
    </div><!-- .topbar -->

    <div class="header__main">
      <div class="header__logo">
        <a href="index-2.html">
          <h1 class="screen-reader-text">Realand</h1>
         <?php echo $this->Html->image('uploads/logo.png');?>
        </a>
      </div><!-- .header__main -->

      <div class="nav-mobile">
        <a href="#" class="nav-toggle">
          <span></span>
        </a><!-- .nav-toggle -->
      </div><!-- .nav-mobile -->

      <div class="header__menu header__menu--v1">
        <ul class="header__nav">
          <li class="header__nav-item">
          
            <?php echo $this->Html->link("Homes", array('controller' => 'Posts','action'=> 'index'), array( 'class' => 'header__nav-link'))?>
          </li>
         <li class="header__nav-item">
          
            <?php echo $this->Html->link("About Us", array('controller' => 'Homes','action'=> 'about'), array( 'class' => 'header__nav-link'))?>
          </li>
            
          </li>
          
          <li class="header__nav-item">
          
            <?php echo $this->Html->link("Ads", array('controller' => 'Posts','action'=> 'index'), array( 'class' => 'header__nav-link'))?>
          </li>
          <li class="header__nav-item">
            <a href="contact.html" class="header__nav-link">Contact Us</a>
            
          </li>
          
          
              
          
            
          <li class="header__nav-item">
          
            <?php echo $this->Html->link("Blog", array('controller' => 'Blogs','action'=> 'index'), array( 'class' => 'header__nav-link'))?>
          </li>
        </ul><!-- .header__nav -->

       
      </div><!-- .header__menu -->
<?php echo $this->Html->link("+  Submit Add", array('controller' => 'posts','action'=> 'add'), array( 'class' => 'header__cta'))?>
  
    </div><!-- .header__main -->
  </div><!-- .container -->
</header><!-- .header -->
<section style="margin-top: 200px;" class="submit-property">
    <div class="container">
      
           
        </ul><!-- .ht-breadcrumb -->

        <div class="submit-property__container">
            <div class="row">
                <div class="col-md-3">
                    <h2 class="bookmarked-listing__headline">Hy   <strong><?php echo $this->Session->read('Auth.User.username') ; ?></strong></h2>
                    <div class="settings-block">
                        <span class="settings-block__title">Manage Account</span>
                        <ul class="settings">
                            <li class="setting"></i><?php echo $this->Html->link("My Profile", array('controller' => 'Users','action'=> 'profile'),array( 'class' => 'setting__link'))?><i class="ion-ios-person-outline setting__icon"></i></a></li>
                            <li class="setting"><a href="bookmarked_listing.html" class="setting__link"><i class="ion-ios-heart-outline setting__icon"></i>Bookmarked Listings</a></li>
                        </ul><!-- settings -->
                    </div><!-- .settings-block -->

                    <div class="settings-block">
                        <span class="settings-block__title">Manage Listing</span>
                        <ul class="settings">
                            <li class="setting"><?php echo $this->Html->link("My Ads", array('controller' => 'posts','action'=> 'myadd'),array( 'class' => 'setting__link'))?><i class="ion-ios-home-outline setting__icon"></i></a></li>
                            <li class="setting setting--current"><?php echo $this->Html->link("Submit New Property", array('controller' => 'posts','action'=> 'add'),array( 'class' => 'setting__link'))?><i class="ion-ios-upload-outline setting__icon"></i></a></li>
                          
                        </ul><!-- settings -->
                    </div><!-- .settings-block -->

                    <div class="settings-block">
                        <ul class="settings">
                            <li class="setting"> <?php echo $this->Html->link("Logout", array('controller' => 'users','action'=> 'logout'),array( 'class' => 'setting__link'))?><i class="ion-ios-undo-outline setting__icon"></i></a></li>
                          
                        </ul><!-- settings -->
                    </div><!-- .settings-block -->
                </div><!-- .col -->

          <?php echo $this->Form->create('Post'); ?>
 
                    <div class="col-md-9">
                        <div class="submit-property__block">
                            <h3 class="submit-property__headline">Basic Information</h3>
                 
                            <div class="submit-property__group">
                               
                                     <?php echo $this->Form->input('title',array('class' => 'submit-property__field')); ?>
                            </div><!-- .submit-property__group -->

                            

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="submit-property__group">
                                      
                                      <?php echo $this->Form->input('price',array('class' => 'submit-property__field')); ?>
                                        <span class="submit-property__unit">USD</span>
                                    </div><!-- .submit-property__group -->
                                </div><!-- .col -->

                                <div class="col-md-4">
                                    <div class="submit-property__group">
                                  
                                            <?php echo $this->Form->input('status',array('class' => 'submit-property__field')); ?>
                                       
                                    </div><!-- .submit-property__group -->
                                </div><!-- .col -->


<div class="col-md-4">
                                    <div class="submit-property__group">
                                 <?php echo $this->Form->input('contact',array('class' => 'submit-property__field')); ?>
                                       
                                    </div><!-- .submit-property__group -->
                                </div><!-- .col -->

                                 <div class="col-md-4">
                                    <div class="submit-property__group">
                                    <?php echo $this->Form->input('qty',array('class' => 'submit-property__field')); ?>
                                       
                                    </div><!-- .submit-property__group -->
                                </div><!-- .col -->

                                
                            </div><!-- .row -->

                      
    <?php echo $this->Form->input('description',array('class' => 'submit-property__field')); ?>
                            <div class="submit-property__group">
                               
                            </div><!-- .submit-property__group -->
                        </div><!-- .submit-property__block -->

                        <div class="submit-property__block">
                            <h3 class="submit-property__headline">Gallery</h3>
                            
    <div class="row">
                                <div class="col-md-5">
                                    <div class="submit-property__group">
                                 
                                        <div class="submit-property__upload">
                                            <input type="file" name="images" id='img' onchange="validateImage()" >
                                            <div class="submit-property__upload-inner">
                                                <span class="ion-ios-plus-outline submit-property__icon"></span>
                                                <span class="submit-property__upload-desc">Drop image here or click to upload</span>
                                            </div>
                                        </div><!-- .submit-proeprty__upload -->
                                    </div><!-- .submit-property__group -->
                                </div><!-- .col -->

                                 <div class="col-md-5">
                                    <div class="submit-property__group">
                                
                                        <div class="submit-property__upload">
                                            <input type="file" name="back_image" id='img' onchange="validateImage()" >
                                            <div class="submit-property__upload-inner">
                                                <span class="ion-ios-plus-outline submit-property__icon"></span>
                                                <span class="submit-property__upload-desc">Drop image here or click to upload</span>
                                            </div>
                                        </div><!-- .submit-proeprty__upload -->
                                    </div><!-- .submit-property__group -->
                                </div><!-- .col -->


                              <div class="col-md-5">
                                    <div class="submit-property__group">
                              
                                        <div class="submit-property__upload">
                                            <input type="file" name="side_image" id='img' onchange="validateImage()" >
                                            <div class="submit-property__upload-inner">
                                                <span class="ion-ios-plus-outline submit-property__icon"></span>
                                                <span class="submit-property__upload-desc">Drop image here or click to upload</span>
                                            </div>
                                        </div><!-- .submit-proeprty__upload -->
                                    </div><!-- .submit-property__group -->
                                </div><!-- .col -->
                           <div class="col-md-5">
                                    <div class="submit-property__group">
                                    
                                        <div class="submit-property__upload">
                                            <input type="file" name="side_image2" id='img' onchange="validateImage()" >
                                            <div class="submit-property__upload-inner">
                                                <span class="ion-ios-plus-outline submit-property__icon"></span>
                                                <span class="submit-property__upload-desc">Drop image here or click to upload</span>
                                            </div>
                                        </div><!-- .submit-proeprty__upload -->
                                    </div><!-- .submit-property__group -->
                                </div><!-- .col -->
                               
                      


<?php echo $this->Form->input('adress',array('class' => 'submit-property__field')); ?>
                      
                                
                                    </div><!-- .submit-property__group -->
                                </div><!-- .col -->
                            </div><!-- .row -->
                        </div><!-- .submit-property__block -->

                  

                       

                        <button type="submit" class="submit-property__submit" style="margin-left:370px;"  >Submit</button>
                    </div><!-- .col -->
                </form>
        
            </div><!-- .row -->
        </div><!-- .submit-property__container -->
    </div><!-- .container -->
</section><!-- .submit-property -->


<a href="#" class="back-to-top"><span class="ion-ios-arrow-up"></span></a>
<div id="submit-property-wysiwyg-icons">
 
</div>

<!-- JS Files -->

    </div>
<a href="#" class="back-to-top"><span class="ion-ios-arrow-up"></span></a>

<script >
   function validateImage() {
    var formData = new FormData();
 
    var file = document.getElementById("img").files[0];
 
    formData.append("Filedata", file);
    var t = file.type.split('/').pop().toLowerCase();
    if (t != "jpeg" && t != "jpg" && t != "png" && t != "bmp" && t != "gif") {
        alert('Please select a valid image file');
        document.getElementById("img").value = '';
        return false;
    }
    if (file.size > 1024000) {
        alert('Max Upload size is 1MB only');
        document.getElementById("img").value = '';
        return false;
    }
    return true;
}
</script>


